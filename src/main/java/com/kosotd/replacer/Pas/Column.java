/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas;

/**
 *
 * @author User
 */
public class Column {
    public String fieldName;
    public String required;
    public String visible;
    public String valueType;
    public String caption;
    public String width;

    public String positionColIndex;
    public String positionRowIndex;
    public String positionBandIndex;

    public void validateFields(){
        if ("".equals(fieldName.trim()) || "".equals(valueType.trim()))
            throw new IllegalArgumentException("field is empty");
        required = "".equals(required.trim()) ? "False" : required;
        visible = "".equals(visible.trim()) ? "True" : visible;
        caption = "".equals(caption.trim()) ? fieldName : caption;
        width = "".equals(width.trim()) ? "-1" : width;
    }

    @Override
    public String toString(){
        String res = "";
        res += "fieldName : " + fieldName + "\n";
        res += "required : " + required + "\n";
        res += "visible : " + visible + "\n";
        res += "valueType : " + valueType + "\n";
        res += "caption : " + caption;
        return res;
    }
}
