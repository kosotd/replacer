
package com.kosotd.replacer.Pas;

import com.kosotd.replacer.Pas.PasHelpers.PasHelper;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasEditor {
    
    public PasEditor(String fileData){
        interfacePart = PasHelper.getInterfacePart(fileData);
        implementationPart = PasHelper.getImplementationPart(fileData);
    }
    
    public boolean existInInterfaceUses(String module){
        return existInUses(module, interfacePart);
    }
    
    public void insertInInterfaceUses(String module){
        interfacePart = insertInUses(module, interfacePart);
    }
    
    public boolean existInImplementationUses(String module){
        return existInUses(module, implementationPart);
    }
    
    public void insertInImplementationUses(String module){
        implementationPart = insertInUses(module, implementationPart);
    }
    
    public void removeFromUses(String module){
        if (existInUses(module, interfacePart))
            interfacePart = removeFromUses(module, interfacePart);
        if (existInUses(module, implementationPart))
            implementationPart = removeFromUses(module, implementationPart);
    }
    
    public String getResult(){
        return interfacePart + implementationPart;
    } 
    
    private String removeFromUses(String module, String data){
        String moduleP = "\\b" + module + "\\b";
        Pattern usesPattern = Pattern.compile("(" + moduleP + "\\s*,)|(,\\s*" + moduleP + ")", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher usesMatcher = usesPattern.matcher(data);
        if (usesMatcher.find())
            data = usesMatcher.replaceAll("");
        return data;
    }
    
    private boolean existInUses(String module, String data){
        Pattern usesPattern = Pattern.compile("\\buses\\b", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher usesMatcher = usesPattern.matcher(data);
        if (usesMatcher.find()){
            int startUses = usesMatcher.end();
            Pattern endUsesPattern = Pattern.compile(";", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
            Matcher endUsesMatcher = endUsesPattern.matcher(data);
            if (endUsesMatcher.find(usesMatcher.end())){
                int endUses = endUsesMatcher.start();
                
                Pattern findModulePattern = Pattern.compile("\\b" + module + "\\b", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
                Matcher findModuleMatcher = findModulePattern.matcher(data);
                if (findModuleMatcher.find(startUses)){
                    if (findModuleMatcher.end() <= endUses)
                        return true;
                }
            }
        }
        return false;
    }
    
    private String insertInUses(String module, String data){
        Pattern usesPattern = Pattern.compile("\\buses\\b", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher usesMatcher = usesPattern.matcher(data);
        boolean insert = true;
        boolean firstUses = false;
        if (!usesMatcher.find()) {
            firstUses = true;
            Pattern usesPosPattern = Pattern.compile("\\b(interface|implementation)\\b", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE); 
            Matcher usesPosMatcher = usesPosPattern.matcher(data);
            if (usesPosMatcher.find()){
                data = new StringBuilder(data).insert(usesPosMatcher.end(), "\n\nuses\n\t;").toString();
                usesMatcher = usesPattern.matcher(data);
                usesMatcher.find();
            } else {
                insert = false;
            }
        }
        if (!insert) return data;
        Pattern endUsesPattern = Pattern.compile(";", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher endUsesMatcher = endUsesPattern.matcher(data);
        if (endUsesMatcher.find(usesMatcher.end())){
            int endPos = endUsesMatcher.start();
            String insStr = firstUses ? module : ", " + module;
            data = new StringBuilder(data).insert(endPos, insStr).toString();
        }
        return data;
    }
    
    private String interfacePart;
    private String implementationPart;
}
