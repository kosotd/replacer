package com.kosotd.replacer.Pas;

import com.kosotd.replacer.FileHelpers.FileHelper;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 *
 * @author User
 */
public class DelphiFileRemover {
    public static void main(String[] args) throws IOException{
        DelphiFileRemover delphiFileRemover = new DelphiFileRemover();
        
        String dir = "C:\\Users\\User\\Desktop\\UrbaniCS\\src";
        String projectFile = "C:\\Users\\User\\Desktop\\UrbaniCS\\SimplifiedUrbaniCS.dpr";
        
        Files.walk(Paths.get(dir))
        .filter(s->s.toAbsolutePath().toString().toLowerCase().endsWith(".pas"))
        .forEach(path -> {
            if (!delphiFileRemover.isFileInProject(path, projectFile))
                try {
                    Files.delete(path);
                } catch (IOException ex) {
                }
        });
        
        Files.walk(Paths.get(dir))
        .filter(s->s.toAbsolutePath().toString().toLowerCase().endsWith(".dfm"))
        .forEach(path -> {
            if (!delphiFileRemover.isFileInProject(path, projectFile))
                try {
                    Files.delete(path);
                } catch (IOException ex) {
                }
        });
    }
    
    private boolean isFileInProject(Path file, String projectFile) {
        String fileNameWithOutExt = FileHelper.getFileNameWithoutExt(file.getFileName().toString());
        Pattern p = Pattern.compile("\\b" + fileNameWithOutExt + "\\b\\s*in\\s*'(.*?)'", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(FileHelper.getFileData(projectFile));
        if (m.find()){
            String pathWithOutExt = FileHelper.getFileNameWithoutExt(file.toAbsolutePath().toString());
            String pathInProjFile = FileHelper.getFileNameWithoutExt(m.group(1));
            return pathWithOutExt.toLowerCase().endsWith(pathInProjFile.toLowerCase());
        }
        return false;
    }
}
