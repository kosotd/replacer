package com.kosotd.replacer.Pas.PasHelpers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;
import javafx.util.Pair;

public class PasHelper {
    
    public static String getModuleName(String fileData) {
        String res = "";
        Pattern moduleNamePattern = Pattern.compile("\\bunit\\s+([a-z0-9_]+)\\b", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE); 
        Matcher moduleNameMatcher = moduleNamePattern.matcher(fileData);
        if (moduleNameMatcher.find())
            res = moduleNameMatcher.group(1);
        return res;
    }
    
    public static String getInterfacePart(String fileData){
        Pattern implPattern = Pattern.compile("implementation", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher implMathcer = implPattern.matcher(fileData);
        String res = fileData;
        if (implMathcer.find())
            res = fileData.substring(0, implMathcer.start());
        return res;
    }
    
    public static String getImplementationPart(String fileData){
        Pattern implPattern = Pattern.compile("implementation", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher implMathcer = implPattern.matcher(fileData);
        String res = fileData;
        if (implMathcer.find())
            res = fileData.substring(implMathcer.start());
        return res;
    }
    
    public static List<String> getDefinedTypesAndFuncs(String fileData){
        List<String> typesAndFuncs = new ArrayList<>();
        String interfacePart = getInterfacePart(fileData);
        List<Pair<Integer, Integer>> typeBound = getDefinedTypes(interfacePart, typesAndFuncs);
        Pattern funcPattern = Pattern.compile("(function|procedure)\\s*([a-z0-9_]+)\\s*(\\(.*?\\))?\\s*(:\\s*[a-z0-9_]+)?\\s*;", 
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        interfacePart = interfacePart.replaceAll("\n", " ");
        Matcher funcMatcher = funcPattern.matcher(interfacePart);
        while (funcMatcher.find()){
            String funcName = funcMatcher.group(2);
            if (typeBound.stream().anyMatch(p -> {
                return (funcMatcher.start() >= p.getKey()) && (funcMatcher.end() <= p.getValue());
            })) continue;
            typesAndFuncs.add(funcName);
        }
        return typesAndFuncs.stream().distinct().collect(Collectors.toList());
    }
    
    private static List<Pair<Integer, Integer>> getDefinedTypes(String fileData, List<String> typesAndFuncs){
        List<Pair<Integer, Integer>> typeBound = new ArrayList<>();
        Pattern typePattern = Pattern.compile("([a-z0-9_]+)\\s*=\\s*(class|interface|array|record|set|function|procedure|dispinterface)", 
             Pattern.CASE_INSENSITIVE | Pattern.MULTILINE); 
        Pattern typeEndPattern = Pattern.compile("\\bend\\b", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher typeMatcher = typePattern.matcher(fileData);
        Matcher typeEndMatcher = typeEndPattern.matcher(fileData);
        while (typeMatcher.find()){
            int typeStartPos = typeMatcher.start();
            String typeName = typeMatcher.group(1);
            int typeEndPos = -1;
            if (typeEndMatcher.find(typeStartPos))
                typeEndPos = typeEndMatcher.end();
            typeBound.add(new Pair<>(typeStartPos, typeEndPos));
            typesAndFuncs.add(typeName);
        }
        return typeBound;
    }

    public static String getInBrackets(String data){
        int startPos = -1, endPos = -1, count = 0;
        for (int i = 0; i < data.length(); ++i)
            if (data.charAt(i) == '('){
                count++;
                startPos = startPos == -1 ? i : startPos;
            } else if (data.charAt(i) == ')'){
                count--;
                endPos = count == 0 ? i : endPos;
                if (count == 0) break;
            }
        return data.substring(startPos + 1, endPos);
    }

    public static String replaceUnicode(String input){
        Pattern p = Pattern.compile("#([0-9]+)\\b");
        Matcher m = p.matcher(input);
        while (m.find()){
            String num = m.group(1);
            Character decoded = (char)Integer.parseInt(num);

            Pattern p1 = Pattern.compile("#(" + num + ")\\b");
            Matcher m1 = p1.matcher(input);
            input = m1.replaceAll(decoded.toString());

            m = p.matcher(input);
        }
        return input;
    }

    public static String getDataInBounds(String data, String type1, String type2, char bound1, char bound2){
        String strPat = "\\b" + type2 + "\\b\\s*=\\s*\\" + bound1 + "(.*?)\\" + bound2;
        if (!type1.equals(""))
            strPat = "\\b" + type1 + "\\b\\s*\\.\\s*" + strPat;
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher m = p.matcher(data);
        if (!m.find()) return "";
        return m.group(1);
    }
    
    public static String getObjectCode(String data, String objectName){
        String strPat = "(^\\s*(inherited|object)\\s*\\b" + objectName + "\\b\\s*:\\s*[a-z0-9_]+\\s*(\\[[0-9]+\\])?\\s*$)";
        Pattern pattern = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(data);
        int pos = data.length();
        if (matcher.find()) pos = matcher.start();
        strPat = "(^\\s*(inherited|object)\\s*[a-z0-9_]+\\s*:\\s*[a-z0-9_]+\\s*(\\[[0-9]+\\])?\\s*$)";
        pattern = Pattern.compile(strPat + "|(^\\s*(end|item)\\s*>?\\s*$)", 
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        matcher = pattern.matcher(data);
        int startPos = -1, count = 0, endPos = 0;
        if (matcher.find(pos))
            do {   
                String str = matcher.group(2) == null ? matcher.group(5) : matcher.group(2);
                switch (str.toLowerCase()){
                    case "inherited" : 
                        startPos = startPos == -1 ? matcher.start() : startPos; 
                        count++; 
                        break;
                    case "object" : 
                        startPos = startPos == -1 ? matcher.start() : startPos; 
                        count++;
                        break;
                    case "item" : 
                        count++; break;
                    case "end" : 
                        count--;
                        endPos = matcher.end();
                        break;
                }
                if (count == 0)
                    return data.substring(startPos, endPos + 1);
            } while (matcher.find());
        return data;
    }
}
