/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas;

import com.kosotd.replacer.FileHelpers.FileHelper;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author User
 */
public class GuidesRemover {
    Guides guides;
    String xmlFileName;
    
    public static void main(String[] args) throws IOException{
        GuidesRemover guidesRemover = new GuidesRemover();
        guidesRemover.loadFromXml("Guides.xml");
        
        String dir = "C:\\Users\\User\\Desktop\\UrbaniCS\\src";
        
        Files.walk(Paths.get(dir))
        .filter(s->s.toAbsolutePath().toString().toLowerCase().endsWith(".pas"))
        .forEach(path -> guidesRemover.removeUses(path.toString()));
    }
    
    public void removeUses(String pasFileName){
        String data = FileHelper.getFileData(pasFileName);
        PasEditor pe = new PasEditor(data);
        for (Guide guide : guides.guides)
            pe.removeFromUses(guide.name);
        FileHelper.setFileData(pasFileName, pe.getResult());
    }
    
    public void loadFromXml(String xmlFileName){
        try {
            JAXBContext jc = JAXBContext.newInstance(Guides.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            guides = (Guides) unmarshaller.unmarshal(new File(xmlFileName));
        } catch (JAXBException e) {
        }
    }
}
