/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas;

import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.Pas.PasForm.IDataSetGetterFinder;
import com.kosotd.replacer.Pas.PasHelpers.PasHelper;
import com.kosotd.replacer.RegexHelpers.ReverseMatcher;
import com.kosotd.replacer.StringHelpers.StringInserter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author User
 */
public class IDataSetDestroyAdder {
    
    private boolean onDestroyExists = true;
    private String formClassName = "";
    
    public static void main(String[] args) throws IOException{
        IDataSetDestroyAdder adder = new IDataSetDestroyAdder();
        
        String dir = "C:\\Users\\User\\Desktop\\UrbaniCS\\src";
        
        Files.walk(Paths.get(dir))
        .filter(s->s.toAbsolutePath().toString().toLowerCase().endsWith(".dfm"))
        .forEach(path -> adder.process(path.toString()));
    }

    private void process(String dfmFileName) {
        String dfmFileData = FileHelper.getFileData(dfmFileName);
        String pasFileName = FileHelper.getFileNameWithoutExt(dfmFileName) + ".pas";
        String pasFileData = FileHelper.getFileData(pasFileName);
        onDestroyExists = true;
        dfmFileData = addOnDestroy(dfmFileData);
        if (!onDestroyExists){
            pasFileData = addOnDestroyInPas(pasFileData);        
            FileHelper.setFileData(dfmFileName, dfmFileData);
            FileHelper.setFileData(pasFileName, pasFileData);
        }
        List<String> dataSets = getDataSets(dfmFileData);
        final StringBuilder sb = new StringBuilder();
        dataSets.stream().forEach(s -> {            
            sb.append(new StringBuilder(s).insert(1, "I").toString() + " := nil;\n");
        });
        String s = sb.toString();
        if (!"".equals(s.trim())){
            pasFileData = insertDataSetsNil(pasFileData, s);
            FileHelper.setFileData(pasFileName, pasFileData);
        }
    }
    
    private String insertDataSetsNil(String pasFileData, String nils){
        Pattern pattern = Pattern.compile("\\bprocedure\\s+\\w+\\s*\\.\\s*FormDestroy\\b", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(pasFileData);
        if (!matcher.find()) throw new AssertionError();
        
        Pattern pattern1 = Pattern.compile("\\bbegin\\b", Pattern.CASE_INSENSITIVE);
        Matcher matcher1 = pattern1.matcher(pasFileData);
        matcher1.find(matcher.start());
        
        Pattern pattern2 = Pattern.compile("\\b(procedure|function)\\s+(\\w+\\s*\\.)?\\s*(\\w+)\\b", Pattern.CASE_INSENSITIVE);
        Matcher matcher2 = pattern2.matcher(pasFileData);
        ReverseMatcher rm = new ReverseMatcher(matcher2);
        if (rm.find(matcher1.start())){
            if (rm.group(2) == null){
                System.out.println(formClassName + "\n" + nils + "\n\n");
            } else {
                StringInserter si = new StringInserter(pasFileData);
                si.insert(matcher1.end(), "\n" + nils);
                si.flush();
                pasFileData = si.getResult();
            }
        }
        return pasFileData;
    }
    
    private List<String> getDataSets(String dfmFileData) {
        Pattern pattern = Pattern.compile("\\b(object|inherited)\\s+(\\w+)\\s*:\\s*TOracleDataSet\\b", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(dfmFileData);
        List<String> result = new ArrayList<>();
        while (matcher.find()){
            result.add(matcher.group(2));
        }
        return result;
    }
    
    private String addOnDestroyInPas(String pasFileData){
        Pattern p = Pattern.compile("\\bprocedure\\s+\\w+\\s*\\.\\s*FormDestroy\\b", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(pasFileData);
        if (m.find()) return pasFileData;
        
        int pos = getEndModulePos(pasFileData);
        StringInserter si = new StringInserter(pasFileData);
        si.insert(pos, 
                "procedure " + formClassName + ".FormDestroy(Sender: TObject);\n" + 
                "begin\n" +
                "inherited;\n" +
                "end;\n\n");
        si.flush();
        pasFileData = si.getResult();
        
        Pattern pattern = Pattern.compile(formClassName + "\\s*=\\s*class\\s*\\(.*?\\)", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(pasFileData);
        if (!matcher.find()) 
            throw new AssertionError();
        pos = matcher.end();
        pattern = Pattern.compile("\\b(strict\\s+)?(private|public|protected)\\b", Pattern.CASE_INSENSITIVE);
        matcher = pattern.matcher(pasFileData);
        int insPos = -1;
        if (!matcher.find(pos)){
            Pattern pattern1 = Pattern.compile("\\bend\\b", Pattern.CASE_INSENSITIVE);
            Matcher matcher1 = pattern1.matcher(pasFileData);
            if (!matcher1.find(pos)) 
                throw new AssertionError();
            insPos = matcher1.start();
        }
        insPos = insPos == -1 ? matcher.start() : insPos;
        si = new StringInserter(pasFileData);
        si.insert(insPos, "procedure FormDestroy(Sender: TObject);\n");
        si.flush();
        pasFileData = si.getResult();
                
        return pasFileData;
    }
    
    private int getEndModulePos(String fileData){
        int res = fileData.length();
        Pattern findEnd = Pattern.compile("\\bend\\b", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Pattern findInit = Pattern.compile("\\binitialization\\b", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher endMatch = findEnd.matcher(fileData);
        Matcher initMatch = findInit.matcher(fileData);
        ReverseMatcher rm = new ReverseMatcher(endMatch);
        ReverseMatcher rim = new ReverseMatcher(initMatch);
        if (rim.find())
            res = rim.start();
        else if (rm.find())                
            res = rm.start();
        return res;
    }
    
    private String addOnDestroy(String dfmFileData){
        Pattern pattern = Pattern.compile("\\bOnDestroy\\s*=\\s*FormDestroy\\b", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(dfmFileData);
        Pattern pattern1 = Pattern.compile("\\b(object|inherited)\\s+\\w+\\s*:\\s*(\\w+)\\b\\s*(\\[[0-9]+\\])?", Pattern.CASE_INSENSITIVE);
        Matcher matcher1 = pattern1.matcher(dfmFileData);
        if (!matcher1.find()) 
            throw new AssertionError();
        formClassName = matcher1.group(2);
        if (!matcher.find()){
            onDestroyExists = false;            
            StringInserter si = new StringInserter(dfmFileData);
            si.insert(matcher1.end(), "\n\tOnDestroy = FormDestroy\n");
            si.flush();
            dfmFileData = si.getResult();
        } 
        return dfmFileData;
    }

}
