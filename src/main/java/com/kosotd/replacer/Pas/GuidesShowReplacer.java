/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas;

import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.FileHelpers.JIniFile;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author User
 */
public class GuidesShowReplacer {
       
    private static Map<String, String> guidesClassNames;
    
    public static void main(String[] args) throws IOException{
        GuidesShowReplacer guidesShowReplacer = new GuidesShowReplacer();
        JIniFile iniFile = new JIniFile("Settings.ini");
        guidesClassNames = iniFile.getSection("GuidesClassNames");
        
        String dir = "C:\\Users\\User\\Desktop\\UrbaniCS\\src";
        
        Files.walk(Paths.get(dir))
        .filter(s->s.toAbsolutePath().toString().toLowerCase().endsWith(".pas"))
        .forEach(path -> guidesShowReplacer.replace(path.toString()));
    }
 
    private void replace(String pasFileName){
        String fileData = FileHelper.getFileData(pasFileName);
        String strPat = "\\bShowModalDialog\\s*\\(\\s*([a-z0-9_]+)\\s*\\)\\s*;";
        Pattern pattern = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher matcher = pattern.matcher(fileData);
        while (matcher.find())
            if (guidesClassNames.containsKey(matcher.group(1))){
                String unitName = guidesClassNames.get(matcher.group(1));
                
                String strPat1 = "\\bShowModalDialog\\s*\\(\\s*(" + matcher.group(1) + ")\\s*\\)\\s*;";
                Pattern pattern1 = Pattern.compile(strPat1, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
                Matcher matcher1 = pattern1.matcher(fileData);
                fileData = matcher1.replaceAll("TGuide.Show('" + unitName + "');");
                
                matcher = pattern.matcher(fileData);
            }
        
        strPat = "\\bSelectFromModalDialog\\s*\\(\\s*([a-z0-9_]+)\\s*,\\s*[^,]+?\\s*,\\s*([^,]+?)\\s*,\\s*([^,]+?)\\s*\\)\\s*=\\s*mrOk\\b";
        pattern = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        matcher = pattern.matcher(fileData);
        while (matcher.find())
            if (guidesClassNames.containsKey(matcher.group(1))){
                String unitName = guidesClassNames.get(matcher.group(1));
                String id = matcher.group(2);
                String title = matcher.group(3);
                
                String strPat1 = "\\bSelectFromModalDialog\\s*\\(\\s*(" + matcher.group(1) + 
                        ")\\s*,\\s*[^,]+?\\s*,\\s*(" + id + ")\\s*,\\s*(" + title + ")\\s*\\)\\s*=\\s*mrOk\\b";
                Pattern pattern1 = Pattern.compile(strPat1, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
                Matcher matcher1 = pattern1.matcher(fileData);
                fileData = matcher1.replaceAll("TGuide.Select('" + unitName + "', " + id + ", " + title + ")");
                
                matcher = pattern.matcher(fileData);
            }   
        
        FileHelper.setFileData(pasFileName, fileData);
    }
}
