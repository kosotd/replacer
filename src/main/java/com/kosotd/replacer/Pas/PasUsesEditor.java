package com.kosotd.replacer.Pas;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.FileHelpers.JIniFile;
import com.kosotd.replacer.Pas.PasHelpers.PasHelper;

public class PasUsesEditor {
    
    public PasUsesEditor(String fileName, String iniFileName){
        fileData = FileHelper.getFileData(fileName);
        moduleName = PasHelper.getModuleName(fileData);
        this.fileName = fileName;
        JIniFile iniFile = new JIniFile(iniFileName);
        names = iniFile.getSection("Names");
        removeNames = iniFile.getSection("RemoveNames").entrySet().stream()
            .map(e -> e.getKey()).collect(Collectors.toList());
    }
    
    public void removeUses(){
        PasEditor pasEditor = new PasEditor(fileData);
        removeNames.stream().forEach(name -> {
            Pattern findNamePattern = Pattern.compile("\\b" + name + "\\b", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
            Matcher findNameMatcher = findNamePattern.matcher(fileData);
            if (findNameMatcher.find())
                pasEditor.removeFromUses(name);
        });
        FileHelper.setFileData(fileName, pasEditor.getResult());
        fileData = pasEditor.getResult();
    }
    
    public void insertUses(){
        String interfacePart = PasHelper.getInterfacePart(fileData);
        PasEditor pasEditor = new PasEditor(fileData);
        names.forEach((p1, p2) -> {
            Pattern findNamePattern = Pattern.compile(":\\s*\\b" + p1 + "\\b", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
            Matcher findNameMatcher = findNamePattern.matcher(interfacePart);
            if (moduleName.toLowerCase().equals(p2.toLowerCase())) return;
            if (findNameMatcher.find())
                if ((!pasEditor.existInInterfaceUses(p2)) && (!pasEditor.existInImplementationUses(p2))){
                    pasEditor.insertInInterfaceUses(p2);
                }
        });
        String implementationPart = PasHelper.getImplementationPart(fileData);
        names.forEach((p1, p2) -> {
            Pattern findNamePattern = Pattern.compile("\\b" + p1 + "\\b", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
            Matcher findNameMatcher = findNamePattern.matcher(implementationPart);
            if (moduleName.toLowerCase().equals(p2.toLowerCase())) return;
            if (findNameMatcher.find())
                if ((!pasEditor.existInInterfaceUses(p2)) && (!pasEditor.existInImplementationUses(p2)))
                    pasEditor.insertInImplementationUses(p2);
        });
        FileHelper.setFileData(fileName, pasEditor.getResult());
        fileData = pasEditor.getResult();
    }
   
    private List<String> removeNames;    
    private Map<String, String> names;
    private final String fileName;
    private final String moduleName;
    private String fileData;     
}
