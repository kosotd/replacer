package com.kosotd.replacer.Pas.PasForm;

import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.FileHelpers.JIniFile;
import com.kosotd.replacer.StringHelpers.StringReplacer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 26.06.2017.
 */
public class ReplaceCallProcedure {

    private static final String workDir = "Z:\\Shared\\dest";
    private static final String srcDir = "Z:\\Shared\\src";


    public static void main(String[] args) throws IOException {
        ReplaceCallProcedure replacer = new ReplaceCallProcedure();

        FileHelper.copyFiles(srcDir, workDir);

        Files.walk(Paths.get(workDir))
                .filter(s -> s.toAbsolutePath().toString().toLowerCase().endsWith(".pas"))
                .forEach(path -> replacer.replace(path.toAbsolutePath().toString()));

    }

    //DataModule1.OksPackage.CallProcedure('AddDocAttribute',[-10,'Количество зданий']);
    private void replace(String pasFileName) {
        String pasFileData = FileHelper.getFileData(pasFileName);

        String strPatt = "\\bDataModule1\\s*\n*\\.\\s*\n*OksPackage\\s*\n*\\.\\s*\n*CallProcedure\\s*\n*\\(([^,]+),(.+?)\\)\\s*(;)?\\s*\n";
        Pattern pattern = Pattern.compile(strPatt, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher matcher = pattern.matcher(pasFileData);
        StringReplacer sr = new StringReplacer();
        while (matcher.find()){
            String procName = matcher.group(1).trim();
            String param = matcher.group(2).trim();
            String end = "\n";
            if (matcher.group(3) != null)
                end = ";\n";
            pasFileData = sr.replase(pasFileData,  matcher.start(), matcher.end(), "CallUniProc(" + procName + ", " + param + ")" + end);
            matcher = pattern.matcher(pasFileData);
        }

        FileHelper.setFileData(pasFileName, pasFileData);
    }
}
