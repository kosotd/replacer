package com.kosotd.replacer.Pas.PasForm;

import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.FileHelpers.JIniFile;
import com.kosotd.replacer.Pas.PasHelpers.PasHelper;
import com.kosotd.replacer.StringHelpers.StringInserter;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VisualComponentsChanger {   
       
    private JIniFile iniFile;
    private Map<String, String> sessions;
    
    public VisualComponentsChanger(JIniFile iniFile){
        this.iniFile = iniFile;
        sessions = iniFile.getSection("OracleSessions");
    }
    
    public void changeButtonsStyle(String dfmFileName){
        String fileData = FileHelper.getFileData(dfmFileName);
        Pattern p = Pattern.compile("\\b(object|inherited)\\b\\s+\\b([0-9a-z_]+)\\b\\s*:\\s*\\bTcxButton\\b\\s*(\\[[0-9]+\\]\\s*)?$", 
                Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher m = p.matcher(fileData);
        StringInserter si = new StringInserter(fileData);
        boolean modify = false;
        while (m.find()){
            modify = true;
            int pos = m.end();
            si.insert(pos, "\nLookAndFeel.Kind = lfStandard\nLookAndFeel.NativeStyle = True");
        }
        si.flush();
        if (modify)
            FileHelper.setFileData(dfmFileName, si.getResult());
    }    
    
    public void addSessionsToDataSets(String pasFileName) {
        File f = new File(pasFileName);
        String pref = FileHelper.getFileNameWithoutExt(f.getName());
        String fileData = FileHelper.getFileData(pasFileName);
        Pattern p = Pattern.compile("\\bfunction\\s+[0-9a-z_]+\\s*\\.\\s*Get([0-9a-z_]+)\\s*\\(\\s*\\)\\s*:\\s*IDataSet\\s*;.*?\\b(end)\\s*;",
                Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher m = p.matcher(fileData);
        StringInserter si = new StringInserter(fileData);
        boolean modify = false;
        while (m.find()){
            modify = true;
            String key = pref + "->" + m.group(1);
            String session = sessions.getOrDefault(key, "");
            session = "".equals(session) ? "OracleSession" : session;
            int pos = m.start(2);
            si.insert(pos, "\tresult.Session :=  MainDataModule." + session + ";\n");
        }
        si.flush();
        if (modify)
            FileHelper.setFileData(pasFileName, si.getResult());
    }
    
    public void getOracleSessions(String dfmFileName){
        File f = new File(dfmFileName);
        String pref = FileHelper.getFileNameWithoutExt(f.getName());
        String fileData = FileHelper.getFileData(dfmFileName);
        Pattern p = Pattern.compile("\\b(object|inherited)\\b\\s+\\b([0-9a-z_]+)\\b\\s*:\\s*\\bTOracleDataSet\\b\\s*(\\[[0-9]+\\]\\s*)?$", 
                Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher m = p.matcher(fileData);
        while (m.find()){
            String code = PasHelper.getObjectCode(fileData, m.group(2));
            Pattern p1 = Pattern.compile("\\bSession\\b\\s*=\\s*\\b(([0-9a-z_]+)(\\s*\\.\\s*([0-9a-z_]+))?)\\b", 
                    Pattern.CASE_INSENSITIVE);
            Matcher m1 = p1.matcher(code);
            if (m1.find()){
                if (!m1.group(1).equals("DataModule1.OracleSession")){
                    if (m1.group(2).equals("DataModule1")){
                        iniFile.writeString("OracleSessions", pref + "->" + m.group(2), m1.group(4));
                    }
                }
            }
        }
        iniFile.updateFile();
    }
}
