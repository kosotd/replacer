package com.kosotd.replacer.Pas.PasForm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.kosotd.replacer.FileHelpers.FileHelper;

public class PasFormReplacer {
          
    private Map<String, List<String>> varNames = new HashMap<>();
    
    private boolean ignoreWordWithFPrefix = false;
    
    public void setIgnoreWordWithFPrefix(boolean ignoreWordWithFPrefix){
        this.ignoreWordWithFPrefix = ignoreWordWithFPrefix;
    }
    
    public void replase(String fileName, String className, String replaseClassName){
        String extension = FileHelper.getExtension(fileName);        
        String replData = "";
        PasRepleaser pr = new PasRepleaser(ignoreWordWithFPrefix);
        DfmReplaser dr = new DfmReplaser(ignoreWordWithFPrefix);
        String name = FileHelper.getFileNameWithoutExt(fileName).toLowerCase();
        
        if (extension.equals("pas")){
            if (varNames.containsKey(name))
                pr.setVarNames(varNames.get(name));
            replData = pr.replease(className, replaseClassName, FileHelper.getFileData(fileName));
        }
        else if (extension.equals("dfm")){
            replData = dr.replease(className, FileHelper.getFileData(fileName));   
            if (!varNames.containsKey(name))
                varNames.put(name, dr.getVarNames()); 
            else
                varNames.get(name).addAll(dr.getVarNames());
        }
        if ((!replData.equals("")) /*&& (pr.isReplase() || dr.isReplase())*/)
            FileHelper.setFileData(fileName, replData);
    }    
}
