/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas.PasForm;

import com.kosotd.replacer.RegexHelpers.ReverseMatcher;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.util.Pair;

public class VariableFinder implements Iterable<VarEntry>, Iterator<VarEntry> {
    Matcher matcher;
    String fileData;
    Pattern findClassName = Pattern.compile("([a-z0-9_]+)\\s*=\\s*class", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
    Pattern findEnd = Pattern.compile("\\bend\\b", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

    @Override
    public Iterator<VarEntry> iterator() {
        return this;
    }

    @Override
    public boolean hasNext() {
        return matcher.find();
    }

    @Override
    public VarEntry next() {
        String statement = matcher.group(1);
        String varName = matcher.group(2);
        String className = "";
        Integer varPos = matcher.start();    
        Integer classEndPos = 0;

        Matcher classMatcher = findClassName.matcher(fileData);
        ReverseMatcher revM = new ReverseMatcher(classMatcher);            
        if (revM.find(varPos)){
            className = revM.group(1);
            Matcher endMatch = findEnd.matcher(fileData);
            if (endMatch.find(revM.end()))
                classEndPos = endMatch.start();
        }       

        return new VarEntry(new Pair<>(statement, className), new Pair<>(varName, new Pair<>(varPos, classEndPos)));
    }

    public VariableFinder(String className, String fileData){
        this.fileData = fileData;
        String strPattern = "(([a-z0-9_]+)\\s*:\\s*" + className + "\\s*)";
        Pattern pattern = Pattern.compile(strPattern, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        matcher = pattern.matcher(fileData);
    }
}
