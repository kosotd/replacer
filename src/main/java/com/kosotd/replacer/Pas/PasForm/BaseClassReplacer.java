/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas.PasForm;

import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.FileHelpers.JIniFile;
import com.kosotd.replacer.Pas.PasEditor;
import com.kosotd.replacer.Pas.PasHelpers.PasHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javafx.util.Pair;

/**
 *
 * @author User
 */
public class BaseClassReplacer {
    private JIniFile iniFile;
    private List<String> customInitFormObjects;
    
    public BaseClassReplacer(JIniFile iniFile){
        this.iniFile = iniFile;
        customInitFormObjects = iniFile.getSection("CustomInitFormObjects").keySet()
            .stream().collect(Collectors.toList());        
    }
    
    private String replaceInherited(String dfmFileData){
        Pattern pattern = Pattern.compile("(^\\s*(inherited|object)\\s*[a-z0-9_]+\\s*:\\s*[a-z0-9_]+\\s*(\\[[0-9]+\\])?\\s*$)|(^\\s*(end|item)\\s*>?\\s*$)", 
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(dfmFileData);
        Stack<Pair<Character, Integer>> stack = new Stack<>();
        while (matcher.find()){   
            for (int i = 0; i < 1; ++i){
                Pair<Character, Integer> pair = null;
                int endPos = 0;
                String str = matcher.group(2) == null ? matcher.group(5) : matcher.group(2);
                switch (str.toLowerCase()){
                    case "inherited" : 
                        stack.push(new Pair<>('i', matcher.start())); break;
                    case "object" : 
                        stack.push(new Pair<>('o', matcher.start())); break;
                    case "item" : 
                        stack.push(new Pair<>('t', matcher.start())); break;
                    case "end" : 
                        pair = stack.pop(); 
                        endPos = matcher.end();
                        break;
                }
                if (pair != null)
                    if (pair.getKey() == 'i'){
                        if (stack.isEmpty()) break;
                        dfmFileData = dfmFileData.substring(0, pair.getValue()) + dfmFileData.substring(endPos);
                        matcher = pattern.matcher(dfmFileData);
                        matcher.find(pair.getValue()); 
                        i--;
                    }
            }
        }
        return dfmFileData;
    }
    
    String baseClassName = "TBaseFormTemplateForm|THelpFormTemplateForm";
    String customUnit = "CustomInitImpl"; 
    String baseUnit = "BaseFormTemplate";
    String helpUnit = "HelpFormTemplate";
    
    private Pair<String, Boolean> replaceBaseClassInPasFile(String pasFileData){
        Pattern pattern = Pattern.compile("([a-z0-9_]+)\\s*=\\s*class\\s*\\(\\s*(" + 
            baseClassName + ")\\s*\\)", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(pasFileData);
        if (matcher.find()){
            String s = matcher.group();
            String s1 = matcher.group(1) + " = class(" +
                "TCustomInitForm)";
            pasFileData = matcher.replaceAll(matcher.group(1) + " = class(" +
                "TCustomInitForm)");
            PasEditor pe = new PasEditor(pasFileData);            
            pe.insertInInterfaceUses(customUnit);
            pe.removeFromUses(helpUnit);
            pe.removeFromUses(baseUnit);
            pasFileData = pe.getResult();
            return new Pair<>(pasFileData, true);
        }
        return new Pair<>(pasFileData, false);
    }
    
    private String replaceOverrideMethods(String pasFileData){
//        Pattern pattern = Pattern.compile("\\b(constructor|function|procedure)\\b.*?\\boverride\\b", Pattern.CASE_INSENSITIVE);
//        Matcher matcher = pattern.matcher(pasFileData);
//        
//        boolean write = false;
//        while (matcher.find()){            
//            if (!write){
//                System.out.println(PasHelper.getModuleName(pasFileData));
//                write = true;
//            }
//            System.out.println(matcher.group());
//        }
//        if (write)
//            System.out.println();
        return pasFileData;
    }
    
    public void replace(String pasFile, String dfmFile){
        String pasFileData = FileHelper.getFileData(pasFile);
        String dfmFileData = FileHelper.getFileData(dfmFile);
        Pair<String, Boolean> pair = replaceBaseClassInPasFile(pasFileData);
        if (!pair.getValue()) return;
        
        replaceOverrideMethods(pasFileData);
        
        pasFileData = pair.getKey();
        dfmFileData = replaceInherited(dfmFileData);        
        Pattern objectP = Pattern.compile("\\b(object\\b\\s*\\b(([a-z0-9_]+)\\b\\s*:\\s*\\b([a-z0-9_]+)))\\b", 
            Pattern.CASE_INSENSITIVE);
        Matcher objectM = objectP.matcher(dfmFileData);
        while (objectM.find()){
            String group = objectM.group(3).toLowerCase();
            if (customInitFormObjects.stream().anyMatch(s -> group.equals(s.toLowerCase()))){
                dfmFileData = dfmFileData.replaceAll(objectM.group(1), "inherited " + objectM.group(2));
                Pattern pasFind = Pattern.compile("\\b" + objectM.group(3) + "\\b\\s*:\\s*\\b" + objectM.group(4) + "\\b\\s*;", 
                    Pattern.CASE_INSENSITIVE);
                Matcher pasMatch = pasFind.matcher(pasFileData);
                pasFileData = pasMatch.replaceAll("");
            }
        }
        FileHelper.setFileData(pasFile, pasFileData);
        FileHelper.setFileData(dfmFile, dfmFileData);
    }
}
