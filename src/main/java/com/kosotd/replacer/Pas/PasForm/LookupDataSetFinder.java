package com.kosotd.replacer.Pas.PasForm;

import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.FileHelpers.JIniFile;
import com.kosotd.replacer.Pas.PasHelpers.PasHelper;
import com.kosotd.replacer.RegexHelpers.ReverseMatcher;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LookupDataSetFinder {
    
    public static void main(String[] args) throws IOException{
        LookupDataSetFinder finder = new LookupDataSetFinder();
        JIniFile iniFile = new JIniFile("DataSets.ini");
        iniFile.clear();
        iniFile.updateFile();
        String dirSrc = "D:\\Src\\Projects\\UrbaniCS\\2.5\\source";
        
        Files.walk(Paths.get(dirSrc))
        .filter(s->s.toAbsolutePath().toString().toLowerCase().endsWith(".dfm"))
        .forEach(path -> finder.storeToIniFromOriginalDfm(path.toString(), iniFile));
        
        String dirDest = "C:\\Users\\User\\Desktop\\UrbaniCS 12\\src";
        
        Files.walk(Paths.get(dirDest))
        .filter(s->s.toAbsolutePath().toString().toLowerCase().endsWith(".dfm"))
        .forEach(path -> finder.checkDestDfm(path.toString(), iniFile));
    }
    
    public void checkDestDfm(String dfmFileName, JIniFile iniFile){
        String dfmFileData = FileHelper.getFileData(dfmFileName);
        File f = new File(dfmFileName);
        String section = FileHelper.getFileNameWithoutExt(f.getName());
        Map<String, String> sect = iniFile.getSection(section);
        
        for (Map.Entry<String, String> entry : sect.entrySet()){
            String objName = entry.getKey();
            String code = PasHelper.getObjectCode(dfmFileData, objName);
            if (code.length() == dfmFileName.length())
                code = PasHelper.getObjectCode(dfmFileData, "F" + objName);
            if (code.length() == dfmFileName.length()) continue;
            String value = entry.getValue();
            String find = value.split("\\s*=\\s*")[0].trim();
            String data = getData(code, "(Properties\\.ListSource|LookupDataSet)");
            if ("".equals(data)) {
                System.out.println(section + " -> " + objName + " [ " + value + " ]");
            }
        }
    }
    
    public void storeToIniFromOriginalDfm(String dfmFileName, JIniFile iniFile){
        String dfmFileData = FileHelper.getFileData(dfmFileName);
        File f = new File(dfmFileName);
        String section = FileHelper.getFileNameWithoutExt(f.getName());
        String strPat = "\\b(Properties\\.ListSource|LookupDataSet)\\b\\s*=\\s*\\b([0-9a-z._]+)\\b";
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(dfmFileData);
        while (m.find()){
            strPat = "\\bobject\\b\\s+([0-9a-z_]+)\\s*:\\s*([0-9a-z_]+)\\b";
            Pattern p1 = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
            Matcher m1 = p1.matcher(dfmFileData);
            ReverseMatcher rm = new ReverseMatcher(m1);
            if (rm.find(m.start())){
                String name = rm.group(1);
                iniFile.writeString(section, name, m.group());
            }
        }
        iniFile.updateFile();
    }
    
    private String getData(String data, String name) {
        String strPat = "\\b" + name + "\\b\\s*=\\s*\\b([0-9a-z._]+)\\b";
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(data);
        if (!m.find()) return "";
        return m.group();
    }
    
}
