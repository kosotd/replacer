package com.kosotd.replacer.Pas.PasForm;

import javafx.util.Pair;

 public class VarEntry extends Pair<Pair<String, String>, Pair<String, Pair<Integer, Integer>>>{
    public VarEntry(Pair<String, String> pair1, Pair<String, Pair<Integer, Integer>> pair2) { super(pair1, pair2); }
    public String getStatement(){ return getKey().getKey(); }            
    public String getVarName(){ return getValue().getKey(); }        
    public String getClassName(){ return getKey().getValue(); }            
    public Integer getVarPos(){ return getValue().getValue().getKey(); } 
    public Integer getClassEndPos(){ return getValue().getValue().getValue(); } 
}
