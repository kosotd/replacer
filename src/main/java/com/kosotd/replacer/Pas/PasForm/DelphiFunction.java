/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas.PasForm;

import com.kosotd.replacer.RegexHelpers.ReverseMatcher;
import com.kosotd.replacer.StringHelpers.StringInserter;
import com.kosotd.replacer.StringHelpers.StringReplacer;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.util.Pair;
import jdk.nashorn.internal.ir.ContinueNode;

/**
 *
 * @author User
 */
public class DelphiFunction {
    
    private List<String> replacedSubFuncs = new ArrayList<>();
    private List<Pair<Integer, Integer>> comments;
    
    public DelphiFunction(){
    }
    
    // not working with subfuncs in subfuncs
    public String restoreFunc(String funcCode){
        Pattern func = Pattern.compile("(function|procedure|constructor|destructor)\\s*([a-z0-9_]+)\\s*(\\.\\s*[a-z0-9_]+)?\\s*(\\(.*?\\))?(\\s*:\\s*[a-z0-9_]+)?\\s*;", 
                Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher funcMatcher = func.matcher(funcCode);
        if (funcMatcher.find()){
            Pattern p = Pattern.compile("[\\n\\s\\r\\t]*");
            Matcher m = p.matcher(funcCode);
            if (m.find(funcMatcher.end()))
                funcCode = funcCode.replaceFirst(m.group(), "\n");
            StringInserter si = new StringInserter(funcCode);
            for (String s : replacedSubFuncs){                
                si.insert(funcMatcher.end(), "\n\n\t" + s + "\n");
            }
            si.flush();
            funcCode = si.getResult();
        }
        return funcCode;
    }
    
    private void getComments(String pasFileData){
        comments = new ArrayList<>();
        Pattern p = Pattern.compile("\\{.*?\\}|'.*?'", Pattern.DOTALL);
        Matcher m = p.matcher(pasFileData);
        while (m.find()){
            comments.add(new Pair<>(m.start(), m.end()));
        }
        p = Pattern.compile("//.*");
        m = p.matcher(pasFileData);
        while (m.find()){
            comments.add(new Pair<>(m.start(), m.end()));
        }
    }
    
    public boolean inComment(int pos){
        return comments.stream().anyMatch(p -> {
            return (pos > p.getKey()) && (pos < p.getValue());
        });
    }
    
    public Pair<String, Pair<Integer, Integer>> getFunctionAtPos(String pasFileData, int pos){
        getComments(pasFileData);
        if (inComment(pos)) return new Pair<>("", new Pair<>(0, 0));
        Pattern procPattern = Pattern.compile("\\b(procedure|function|constructor|destructor)\\b.*", Pattern.CASE_INSENSITIVE);
        Matcher procMatcher = procPattern.matcher(pasFileData);
        ReverseMatcher reverseMatcher = new ReverseMatcher(procMatcher);
        int funcStart = 0, funcEnd = 0;
        if (reverseMatcher.find(pos))
            funcStart = reverseMatcher.start();
        Pattern pattern = Pattern.compile("\\b(begin|end|try|case)\\b(\\s*;)?", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(pasFileData);
        int pairCount = 0;
        int startFind = funcStart;
        while (matcher.find(startFind)){
            if (inComment(matcher.start())){
                startFind = matcher.end();
                continue;
            }
            if (matcher.group().toLowerCase().equals("begin")){
                pairCount++;
                break;
            }
        }
        while (matcher.find()){
            if (inComment(matcher.start()))
                continue;
            String str = matcher.group(1);
            switch (str.toLowerCase()){
                case "begin" :
                    pairCount++;
                    break;
                case "try" :
                    pairCount++;
                    break;
                case "case" :
                    pairCount++;
                    break;
                case "end" :
                    pairCount--;
                    funcEnd = matcher.end();
                    break;
            }
            if (pairCount == 0){
                if ((pos > funcStart) && (pos < funcEnd)) {
                    int origFuncEnd = funcEnd;
                    for (String s : replacedSubFuncs)
                        origFuncEnd += s.length();
                    return new Pair<>(pasFileData.substring(funcStart, funcEnd), new Pair<>(funcStart, origFuncEnd));
                } else {
                    StringReplacer sr = new StringReplacer();
                    String subFunc = pasFileData.substring(funcStart, funcEnd);
                    replacedSubFuncs.add(subFunc);
                    pasFileData = sr.replase(pasFileData, funcStart, funcEnd, "");
                    return getFunctionAtPos(pasFileData, pos - subFunc.length());
                }
            }
        }
        return new Pair<>("", new Pair<>(0, 0));
    }
    
    public String insertAfterBegin(String funcCode, String inserting) {
        getComments(funcCode);
        Pattern pattern = Pattern.compile("\\bbegin\\b", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(funcCode);
        while (matcher.find()){
            if (inComment(matcher.start())) continue;
            StringInserter si = new StringInserter(funcCode);
            int insPos = matcher.end();
            si.insert(insPos, "\n\t" + inserting);
            si.flush();
            funcCode = si.getResult();
            break;
        }
        return funcCode;
    }

    public String insertIntoVar(String funcCode, String inserting){
        getComments(funcCode);
        Pattern p = Pattern.compile(inserting, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(funcCode);
        if (m.find())
            return funcCode;
        Pattern func = Pattern.compile("(function|procedure|constructor|destructor)\\s*([a-z0-9_]+)\\s*(\\.\\s*[a-z0-9_]+)?\\s*\\(.*?\\)(\\s*:\\s*[a-z0-9_]+)?\\s*;", 
                Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher funcMatcher = func.matcher(funcCode);
        int startFind = 0;
        if (funcMatcher.find()) 
            startFind = funcMatcher.end();
        Pattern pattern = Pattern.compile("\\b(var|begin)\\b", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(funcCode);
        while (matcher.find(startFind)){
            if (inComment(matcher.start())){
                startFind = matcher.end();
                continue;
            }
            String str = matcher.group(1).toLowerCase();
            StringInserter si = new StringInserter(funcCode);
            int insPos;
            boolean insVar = false;
            if (str.equals("begin")) {
                insVar = true;
                inserting += "\n";
                insPos = matcher.start();
            } else {
                insPos = matcher.end();
            }
            si.insert(insPos, inserting);
            if (insVar)
                si.insert(insPos, "var");
            si.flush();
            return si.getResult();
        }
        return funcCode;
    }
}
