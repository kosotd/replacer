package com.kosotd.replacer.Pas.PasForm;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DfmReplaser {
    boolean replase;
    List<String> varNames = new ArrayList<>();
    private boolean ignoreWordWithFPrefix = false;
    
    public DfmReplaser(boolean ignoreWordWithFPrefix){
        this.ignoreWordWithFPrefix = ignoreWordWithFPrefix;
    }
    
    public boolean isReplase(){
        return replase;
    }
    
    public List<String> getVarNames(){
        return varNames;
    }
    
    public String replease(String className, String fileData){
        String strPattern = "object\\s*([a-z0-9_]+)\\s*:\\s*" + className;      
        Pattern pattern = Pattern.compile(strPattern, Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(fileData);
        replase = false;
        while (matcher.find()){            
            String name = matcher.group(1);
            
            if (ignoreWordWithFPrefix)
                if ((name.charAt(0) == 'F') || (name.charAt(0) == 'f'))
                    continue;
            
            replase = true;            
            varNames.add(name);
            fileData = fileData.replaceAll("\\b" + name + "\\b", "F" + name);
        }        
        return fileData;
    }
}
