/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas.PasForm;

import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.FileHelpers.JIniFile;
import com.kosotd.replacer.RegexHelpers.ReverseMatcher;
import com.kosotd.replacer.StringHelpers.StringInserter;
import com.kosotd.replacer.StringHelpers.StringReplacer;
import javafx.util.Pair;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author User
 */
public class PropertiesEditor {
    private JIniFile iniFile;
    private Map<String, String> settingsManagerProperties;
    
    public PropertiesEditor(JIniFile iniFile){
        this.iniFile = iniFile;
        settingsManagerProperties = iniFile.getSection("SettingsManagerProperties");
    }
    
    public void replace(String pasFileName){
        String pasFileData = FileHelper.getFileData(pasFileName);
        for (Map.Entry<String, String> entry : settingsManagerProperties.entrySet()){
            pasFileData = replaceProperty(pasFileData, entry.getKey(), entry.getValue());
        }
        FileHelper.setFileData(pasFileName, pasFileData);
    }

    private String replaceProperty(String pasFileData, String property, String type) {
        String strPattern = "\\bGetManagers.SettingsManager." + property + "\\b";
        String strPattern1 = "\\bGetManagers.MapManager." + property + "\\b";
        String strPattern2 = "\\bGetManagers.ConnectionManager." + property + "\\b";
        String strPattern3 = "\\bGetManagers.LayerManager." + property + "\\b";
        Pattern pattern = Pattern.compile(strPattern + "|" + strPattern1 + "|" + strPattern2 + "|" + strPattern3, 
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(pasFileData);
        while (matcher.find()) {
            DelphiFunction function = new DelphiFunction();
            Pair<String, Pair<Integer, Integer>> func = function.getFunctionAtPos(pasFileData, matcher.start());
            String funcCode = func.getKey();
            if (funcCode.trim().equals("")) continue;
            String before = funcCode;
            funcCode = function.insertIntoVar(funcCode, "\n\t" + type.substring(1) + " : " + type +  ";");
            if (!before.equals(funcCode))
                funcCode = function.insertAfterBegin(funcCode,
                        type.substring(1) + " := ClassFactory.GetSettingsManager.GetProperties(" +
                                type.substring(1) + "ClassName) as " + type + ";");
            Matcher m = pattern.matcher(funcCode);
            funcCode = m.replaceAll(type.substring(1) + "." + property);
            funcCode = function.restoreFunc(funcCode);
            StringReplacer sr = new StringReplacer();
            pasFileData = sr.replase(pasFileData, func.getValue().getKey(), func.getValue().getValue(), funcCode);
            matcher = pattern.matcher(pasFileData);
        }
        return pasFileData;
    }
}
