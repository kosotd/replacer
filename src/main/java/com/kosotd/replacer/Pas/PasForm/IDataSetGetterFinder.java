/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas.PasForm;

import com.kosotd.replacer.FileHelpers.FileHelper;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author User
 */
public class IDataSetGetterFinder {
    public static void main(String[] args) throws IOException{
        IDataSetGetterFinder finder = new IDataSetGetterFinder();
        
        String dir = "C:\\Users\\User\\Desktop\\UrbaniCS 12\\src";
        
        Files.walk(Paths.get(dir))
        .filter(s->s.toAbsolutePath().toString().toLowerCase().endsWith(".pas"))
        .forEach(path -> finder.checkPas(path.toString()));
    }

    private void checkPas(String pasFileName) {
        String pasFileData = FileHelper.getFileData(pasFileName);
        Pattern p = Pattern.compile("\\bfunction\\s+[0-9a-z_]+\\s*\\.\\s*Get([0-9a-z_]+)\\s*(\\(\\s*\\)\\s*)?:\\s*IDataSet\\s*;.*?\\b(end)\\s*;",
            Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher m = p.matcher(pasFileData);
        while (m.find()){
            String code = m.group();
            String data = getData(code, "result\\.Session");
            if ("".equals(data)){
                System.out.println(pasFileName + " ->\n" + code);
            }
        }
    }
    
    private String getData(String data, String name) {
        String strPat = "\\b" + name + "\\b\\s*:=\\s*\\b([0-9a-z._]+)\\b";
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(data);
        if (!m.find()) return "";
        return m.group();
    }
}
