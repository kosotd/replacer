package com.kosotd.replacer.Pas.PasForm;

import com.kosotd.replacer.FileHelpers.FileHelper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 26.06.2017.
 */
public class SqlParamsInserter {

    private static final String sqlFile = "Z:\\Shared\\sql\\sql.txt";
    private static final String paramsFile = "Z:\\Shared\\sql\\params.txt";
    private static final String resultFile = "Z:\\Shared\\sql\\result.txt";

    public static void main(String[] args) throws IOException {
        String sql = FileHelper.getFileData(sqlFile);
        String params = FileHelper.getFileData(paramsFile);

        Map<String, String> mapParams = parseParams(params);
        for (Map.Entry<String, String> entry : mapParams.entrySet()){
            Pattern pattern = Pattern.compile(":" + entry.getKey() + "\\b", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(sql);
            sql = matcher.replaceAll(entry.getValue());
        }

        FileHelper.setFileData(resultFile, sql);
    }

    private static Map<String, String> parseParams(String params) {
        Map<String, String> mapParams = new HashMap<>();
                                              // :WORKER_ID(Float)=8
        Pattern pattern = Pattern.compile(":(\\w+)\\b.*=(.*)", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(params);
        while (matcher.find()){
            String name = matcher.group(1);
            String value = replaceValue(matcher.group(2).trim());
            mapParams.put(name, value);
        }
        return mapParams;
    }

    private static String replaceValue(String value) {
        if (value.equalsIgnoreCase("<NULL>")){
            return "null";
        }
        Pattern pattern = Pattern.compile("[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}");
        Matcher matcher = pattern.matcher(value);
        if (matcher.matches()){
            return "'" + value + "'::timestamp";
        }

        return value;
    }

}
