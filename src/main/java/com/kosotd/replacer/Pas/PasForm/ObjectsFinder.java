/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas.PasForm;

import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.FileHelpers.JIniFile;
import com.kosotd.replacer.Pas.PasHelpers.PasHelper;
import com.kosotd.replacer.RegexHelpers.ReverseMatcher;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author User
 */
public class ObjectsFinder {
    public static void main(String[] args) throws IOException{
        ObjectsFinder finder = new ObjectsFinder();
//        String dirSrc = "D:\\Src\\Projects\\UrbaniCS\\2.5\\source";
//        
//        Files.walk(Paths.get(dirSrc))
//        .filter(s->s.toAbsolutePath().toString().toLowerCase().endsWith(".dfm"))
//        .forEach(path -> {
//            try {
//                finder.storeToIniFromOriginalDfm(path.toString());
//            } catch (IOException ex) {
//            }
//        });
        
        String dirDest = "C:\\Users\\User\\Desktop\\UrbaniCS\\src";
        
        Files.walk(Paths.get(dirDest))
        .filter(s->s.toAbsolutePath().toString().toLowerCase().endsWith(".dfm"))
        .forEach(path -> {
            try {
                finder.checkDestDfm(path.toString());
            } catch (IOException ex) {
            }
        });
    }
    
    public void checkDestDfm(String dfmFileName) throws IOException{
        String dfmFileData = FileHelper.getFileData(dfmFileName);
        File f = new File(dfmFileName);
        String sectionName = FileHelper.getFileNameWithoutExt(f.getName());
        String path = new File(".").getCanonicalPath();
        JIniFile iniFile = new JIniFile(path + "\\IniFiles\\" + sectionName + ".ini");
        List<String> sections = iniFile.readSections();
        sections.stream().forEach(section -> {
            Map<String, String> sect = iniFile.getSection(section);
            String code = PasHelper.getObjectCode(dfmFileData, section);
            for (Map.Entry<String, String> entry : sect.entrySet()){                
                String strPat1 = "\\b(" + entry.getKey() + ")\\s*=.+";
                Pattern p1 = Pattern.compile(strPat1, Pattern.CASE_INSENSITIVE);
                Matcher m1 = p1.matcher(code);
                if (checkValue(entry.getValue()))
                    if (!m1.find()){
                        System.out.println(sectionName + " -> " + section + " [ " + entry.getValue() + " ]");
                    }
            }        
        });
    }
    
    public boolean checkValue(String value){
        if (value.toLowerCase().startsWith("onkeydown")) return false;
        if (value.toLowerCase().startsWith("visible")) return false;
        if (value.toLowerCase().startsWith("margins")) return false;
        if (value.toLowerCase().startsWith("bitmap")) return false;
        if (value.toLowerCase().startsWith("items")) return false;
        if (value.toLowerCase().startsWith("columns")) return false;
        if (value.toLowerCase().startsWith("focuscontrol")) return false;
        if (value.toLowerCase().startsWith("stylehot")) return false;
        if (value.toLowerCase().startsWith("stylefocused")) return false;
        if (value.toLowerCase().startsWith("styledisabled")) return false;
        if (value.toLowerCase().startsWith("style")) return false;
        if (value.toLowerCase().startsWith("onexecute")) return false;
        if (value.toLowerCase().startsWith("item")) return false;
        if (value.toLowerCase().startsWith("bars")) return false;
        if (value.toLowerCase().startsWith("action")) return false;
        if (value.toLowerCase().startsWith("caption")) return false;
        if (value.toLowerCase().startsWith("properties")) return false;
        if (value.toLowerCase().startsWith("styles")) return false;
        if (value.toLowerCase().startsWith("onclick")) return false;
        if (value.toLowerCase().startsWith("onupdate")) return false;
        if (value.toLowerCase().startsWith("lookandfeel.kind")) return false;
        if (value.toLowerCase().startsWith("lookandfeel.nativestyle")) return false;
        if (value.toLowerCase().startsWith("session")) return false;
        if (value.toLowerCase().startsWith("explicitleft")) return false;
        if (value.toLowerCase().startsWith("explicitwidth")) return false;
        if (value.toLowerCase().startsWith("explicittop")) return false;
        if (value.toLowerCase().startsWith("explicitheight")) return false;
        if (value.toLowerCase().startsWith("controldata")) return false;
        if (value.toLowerCase().startsWith("align")) return false;
        if (value.toLowerCase().startsWith("anchors")) return false;
        if (value.toLowerCase().startsWith("taborder")) return false;
        if (value.toLowerCase().startsWith("height")) return false;
        if (value.toLowerCase().startsWith("width")) return false;
        if (value.toLowerCase().startsWith("top")) return false;
        if (value.toLowerCase().startsWith("left")) return false;
        return true;
    }
    
    public void storeToIniFromOriginalDfm(String dfmFileName) throws IOException{
        String dfmFileData = FileHelper.getFileData(dfmFileName);
        File f = new File(dfmFileName);
        String section = FileHelper.getFileNameWithoutExt(f.getName());
        String path = new File(".").getCanonicalPath();
        JIniFile iniFile = new JIniFile(path + "\\IniFiles\\" + section + ".ini");
        String strPat = "\\b(object|inherited)\\s+(\\w+)\\s*:\\s*(\\w+)\\b";
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(dfmFileData);
        while (m.find()){
            if (m.start() == 0) continue;
            String code = PasHelper.getObjectCode(dfmFileData, m.group(2));
            String strPat1 = "\\b([a-z0-9_.]+)\\s*=.+";
            Pattern p1 = Pattern.compile(strPat1, Pattern.CASE_INSENSITIVE);
            Matcher m1 = p1.matcher(code);
            while (m1.find()){
                String name = m.group(2);
                iniFile.writeString(name, m1.group(1), m1.group());
            }
        }
        iniFile.updateFile();
    }
    
    private String getData(String data, String name) {
        String strPat = "\\b" + name + "\\b\\s*=\\s*\\b([0-9a-z._]+)\\b";
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(data);
        if (!m.find()) return "";
        return m.group();
    }
}
