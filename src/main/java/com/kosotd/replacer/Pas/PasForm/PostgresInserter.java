package com.kosotd.replacer.Pas.PasForm;

import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.FileHelpers.JIniFile;
import com.kosotd.replacer.Pas.PasEditor;
import com.kosotd.replacer.Pas.PasHelpers.PasHelper;
import com.kosotd.replacer.RegexHelpers.ReverseMatcher;
import com.kosotd.replacer.StringHelpers.StringInserter;
import com.kosotd.replacer.StringHelpers.StringReplacer;
import javafx.util.Pair;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by User on 04.05.2017.
 */
public class PostgresInserter {

    private final List<String> dataSets = new ArrayList<String>(){{
        add("EnergyVoltagesDS");
        add("EnergyLepLocationsDS");
        add("EnergyStatesDS");
        add("EnergyExploitersDS");
    }};
    private static final String iniFileName = "IniFile.ini";
    private static final String workDir = "Z:\\Shared\\dest";
    private static final String srcDir = "Z:\\Shared\\src";
    private static final String dataModulesPath = "Z:\\Shared\\dm";
    private static final JIniFile iniFile = new JIniFile(iniFileName);
    private static final boolean debugMode = false;
    private static final boolean groupDefines = true;
    private String className = null;
    private final boolean someDataSets = false;

    public static void main(String[] args) throws IOException {
        PostgresInserter inserter = new PostgresInserter();

        FileHelper.copyFiles(srcDir, workDir);

        Files.walk(Paths.get(workDir))
                .filter(s -> s.toAbsolutePath().toString().toLowerCase().endsWith(".dfm"))
                .forEach(path -> inserter.insert(path.toAbsolutePath().toString()));
//        storeDataSetsToIniFile(dataModulesPath);
    }

    private static void storeDataSetsToIniFile(String dmPath) throws IOException {
        iniFile.eraseSection("GlobalDataSets");
        Files.walk(Paths.get(dmPath))
            .filter(s -> s.toAbsolutePath().toString().toLowerCase().endsWith(".dfm"))
            .forEach(path -> {
                String dfmFileData = FileHelper.getFileData(path.toAbsolutePath().toString());
                List<OraDataSet> dataSets = new ArrayList<>();
                Pattern pattern = Pattern.compile("\\bobject\\s+([a-z0-9_]+)\\s*:\\s*(TOracleDataSet|TOracleQuery)\\b", Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(dfmFileData);
                while (matcher.find()) {
                    String name = matcher.group(1);
                    iniFile.writeString("GlobalDataSets", name, path.getFileName().toString());
                }
                iniFile.updateFile();
            });
    }

    private void insert(String dfmFileName) {
        String pasFileName = dfmFileName.substring(0, dfmFileName.length() - 4) + ".pas";
        List<OraDataSet> oraDataSets = findOraDataSets(dfmFileName);
        generateUniDataSets(dfmFileName, pasFileName, oraDataSets);
        if (groupDefines)
            addPostgresDefinesGroup(pasFileName, oraDataSets);
        else
            addPostgresDefines(pasFileName, oraDataSets);
        if (!debugMode)
            checkDataSets(pasFileName, oraDataSets);
        addDataSetsToFormCreate(pasFileName, oraDataSets);

        PasEditor pe = new PasEditor(FileHelper.getFileData(pasFileName));
        if (!pe.existInInterfaceUses("Uni"))
            pe.insertInInterfaceUses("Uni");
        FileHelper.setFileData(pasFileName, pe.getResult());
    }

    private void addDataSetsToFormCreate(String pasFileName, List<OraDataSet> oraDataSets) {
        createFormCreateIfNotExist(pasFileName);
        String pasFileData = FileHelper.getFileData(pasFileName);
        String pattStr = "\\bprocedure\\s+\\w+\\s*\\.\\s*FormCreate\\s*\\(.*?\\)\\s*;.*";
        Pattern pattern = Pattern.compile(pattStr, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(pasFileData);
        if (!matcher.find()) throw new RuntimeException("FormCreate not found");
        int insertPos = getInsPos(pasFileData, matcher.end());
        StringBuilder sb = new StringBuilder();
        sb.append("\n{$IFDEF POSTGRES}\n");
        for (OraDataSet ds : oraDataSets){
            if (!"".equals(ds.dataSource)) {
                sb.append("\t" + ds.dataSource + ".DataSet := ");
                sb.append("uni_" + ds.name);
                sb.append(";\n\t");
            } else
                sb.append("\t");
            sb.append(ds.name); sb.append(".Free;\n");
        }
        sb.append("{$ELSE}\n");
        for (OraDataSet ds : oraDataSets) {
            sb.append("\tuni_" + ds.name);
            sb.append(".Free;\n");
        }
        sb.append("{$ENDIF}");
        StringInserter si = new StringInserter(pasFileData);
        si.insert(insertPos, sb.toString());
        si.flush();
        if (debugMode)
            System.out.println(si.getResult());
        else
            FileHelper.setFileData(pasFileName, si.getResult());
    }

    private int getInsPos(String pasFileData, int formCreatePos){
        Pattern pattern = Pattern.compile("\\b(procedure|function|begin)\\b", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(pasFileData);
        if (matcher.find(formCreatePos)){
            String finded = matcher.group(1);
            if (!"begin".equalsIgnoreCase(finded)) throw new RuntimeException("Form create contains nested procedure");
            return matcher.end();
        }
        throw new RuntimeException("End of class not found");
    }

    private void createFormCreateIfNotExist(String pasFileName) {
        String pasFileData = FileHelper.getFileData(pasFileName);
        String pattStr = "\\bprocedure\\s+\\w+\\s*\\.\\s*FormCreate\\s*\\(.*?\\)\\s*;.*";
        Pattern pattern = Pattern.compile(pattStr, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(pasFileData);
        if (!matcher.find()) {
            pattern = Pattern.compile("\\b" + className + "\\s*=\\s*class\\s*\\(\\w+\\)\\s*", Pattern.CASE_INSENSITIVE);
            matcher = pattern.matcher(pasFileData);
            if (!matcher.find()) throw new RuntimeException("Class not found");
            int classEndPos = matcher.end();
            pattern = Pattern.compile("\\b(end|private|public|protected)\\b", Pattern.CASE_INSENSITIVE);
            matcher = pattern.matcher(pasFileData);
            if (!matcher.find(classEndPos)) throw new RuntimeException("End class not found");
            int insPosInClass = matcher.start();
            StringInserter si = new StringInserter(pasFileData);
            si.insert(insPosInClass, "\tprocedure FormCreate(Sender : TObject);\n");

            /* возможно будет initialization */
            Pattern endPattern = Pattern.compile("\\bend\\b", Pattern.CASE_INSENSITIVE);
            Matcher endMatcher = endPattern.matcher(pasFileData);
            ReverseMatcher reverseMatcher = new ReverseMatcher(endMatcher);
            if (!reverseMatcher.find()) throw new RuntimeException("End not found");
            int insPos = reverseMatcher.start();
            si.insert(insPos,
                    "\nprocedure " + className + ".FormCreate(Sender: TObject);\n" +
                            "begin\n" +
                            "\tinherited;\n" +
                            "end;\n\n");
            si.flush();
            FileHelper.setFileData(pasFileName, si.getResult());
        }
    }

    private void checkDataSets(String pasFileName, List<OraDataSet> oraDataSets){
        String pasFileData = FileHelper.getFileData(pasFileName);
        String implPart = PasHelper.getImplementationPart(pasFileData);
        getComments(implPart);
        List<Pair<Integer, Integer>> defines = getDefines(implPart);

        List<String> globalDataSets = iniFile.getSection("GlobalDataSets").entrySet().stream().map(e -> e.getKey()).collect(Collectors.toList());
        for (OraDataSet ds : oraDataSets)
            globalDataSets.add(ds.name);

        for (String name : globalDataSets) {
            Pattern pattern = Pattern.compile("\\b" + name + "\\b", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(implPart);
            while (matcher.find()) {
                if (inComment(matcher.start())) continue;
                if (!defines.stream().anyMatch(p -> (matcher.start() > p.getKey()) && (matcher.start() < p.getValue())))
                    throw new RuntimeException(name + " not in defines");
            }
        }
    }

    private List<Pair<Integer, Integer>> getDefines(String pasFileData){
        List<Pair<Integer, Integer>> defines = new ArrayList<>();
        Pattern p = Pattern.compile("\\{\\$IFDEF POSTGRES\\}.*?\\{\\$ENDIF\\}", Pattern.DOTALL);
        Matcher m = p.matcher(pasFileData);
        while (m.find()){
            defines.add(new Pair<>(m.start(), m.end()));
        }
        return defines;
    }

    private String replaceOraFuncToPostgres(String data) {

        //Result := ParamByName('ID').Value;
        //Result := GetVariable('ID');

        //uni_DocTemplatesDataSet.LockMode := TLockMode.lmNone;
        //LockingMode := lmNone;

        String strPatt = "\\bSetVariable\\s*\\(([^,]+),(.+)\\)\\s*(;)?\\s*\n";
        Pattern pattern = Pattern.compile(strPatt, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(data);
        StringReplacer sr = new StringReplacer();
        while (matcher.find()){
            String paramName = matcher.group(1).trim();
            String value = matcher.group(2).trim();
            String end = "\n";
            if (matcher.group(3) != null)
                end = ";\n";
            data = sr.replase(data, matcher.start(), matcher.end(), "ParamByName(" + paramName + ").Value := " + value + end);
            matcher = pattern.matcher(data);
        }

        strPatt = "\\bDeleteVariables\\b";
        pattern = Pattern.compile(strPatt, Pattern.CASE_INSENSITIVE);
        matcher = pattern.matcher(data);
        while (matcher.find()){
            data = sr.replase(data, matcher.start(), matcher.end(), "Params.Clear");
            matcher = pattern.matcher(data);
        }

        strPatt = "\\bSession\\b";
        pattern = Pattern.compile(strPatt, Pattern.CASE_INSENSITIVE);
        matcher = pattern.matcher(data);
        while (matcher.find()){
            data = sr.replase(data, matcher.start(), matcher.end(), "Connection");
            matcher = pattern.matcher(data);
        }

        strPatt = "\\bDeclareAndSet\\s*\\(([^,]+),([^,]+),(.+?)\\)\\s*(;)?\\s*\n";
        pattern = Pattern.compile(strPatt, Pattern.CASE_INSENSITIVE);
        matcher = pattern.matcher(data);
        while (matcher.find()){
            String paramName = matcher.group(1).trim();
            String paramType = matcher.group(2).trim();
            String value = matcher.group(3).trim();
            String end = "\n";
            if (matcher.group(4) != null)
                end = ";\n";
            data = sr.replase(data, matcher.start(), matcher.end(), "ParamByName(" + paramName + ")." +
                    getType(paramType) + " := " + value + end);
            matcher = pattern.matcher(data);
        }

        strPatt = "\\bDeclareVariable\\s*\\(([^,]+),(.+?)\\)\\s*(;)?\\s*\n";
        pattern = Pattern.compile(strPatt, Pattern.CASE_INSENSITIVE);
        matcher = pattern.matcher(data);
        while (matcher.find()){
            String paramName = matcher.group(1).trim();
            String paramType = matcher.group(2).trim();
            String end = "\n";
            if (matcher.group(3) != null)
                end = ";\n";
            data = sr.replase(data, matcher.start(), matcher.end(), "Params.CreateParam(" +
                    getFieldType(paramType) + ", " + paramName + ", TParamType.ptInput)" + end);
            matcher = pattern.matcher(data);
        }

        strPatt = "\\bSearchRecord\\s*\\(([^,]+),([^,]+),(.+?)\\)\\s*(;)?\\s*\n";
        pattern = Pattern.compile(strPatt, Pattern.CASE_INSENSITIVE);
        matcher = pattern.matcher(data);
        while (matcher.find()){
            String recordName = matcher.group(1).trim();
            String recordValue = matcher.group(2).trim();
            String options = matcher.group(3).trim();
            String end = ")\n";
            if (matcher.group(4) != null)
                end = ");\n";
            data = sr.replase(data, matcher.start(), matcher.end(),
                    "Locate(" + recordName + ", " + recordValue + ", " + replaceOptions(options) + end);
            matcher = pattern.matcher(data);
        }

        return data;
    }

    private String replaceOptions(String options){
        String result = "[";
        Pattern pattern = Pattern.compile("(?i)\\bsrIgnoreCase\\b");
        Matcher matcher = pattern.matcher(options);
        if (matcher.find())
            result += "loCaseInsensitive";

        pattern = Pattern.compile("(?i)\\bsrPartialMatch\\b");
        matcher = pattern.matcher(options);
        if (matcher.find())
            result += ", loPartialKey";

        result += "]";
        return result;
    }

    private String getFieldType(String type){
        switch (type.toLowerCase().trim()){
            case "otstring" : return "ftString";
            case "otinteger" : return "ftInteger";
            case "otdate" : return "ftDate";
            case "otblob" : return "ftBlob";
        }
        throw new RuntimeException("Unknown type: " + type);
    }

    private String getType(String type){
        switch (type.toLowerCase().trim()){
            case "otstring" : return "asString";
            case "otinteger" : return "asInteger";
            case "otdate" : return "asDate";
            case "otsubst" : return "Value";
        }
        throw new RuntimeException("Unknown type: " + type);
    }

    private void addPostgresDefines(String pasFileName, List<OraDataSet> oraDataSets) {
        String pasFileData = FileHelper.getFileData(pasFileName);
        String intfPart = PasHelper.getInterfacePart(pasFileData);
        String implPart = PasHelper.getImplementationPart(pasFileData);
        for (OraDataSet ds : oraDataSets) {
            String name = ds.name;
            Pattern pattern = Pattern.compile(".*\\b" + name + "\\b.*", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(implPart);
            StringReplacer stringReplacer = new StringReplacer();
            while (matcher.find()) {
                int start = matcher.start();
                int end = matcher.end();
                String oraStr = matcher.group();
                String postgresStr = oraStr.replaceAll("(?i)\\b" + name + "\\b", "uni_" + name);
                String repl =
                        "{$IFDEF POSTGRES}\n" +
                        postgresStr +
                        "{$ELSE}\n" +
                        oraStr +
                        "{$ENDIF}";

                implPart = stringReplacer.replase(implPart, start, end, repl);
                matcher = pattern.matcher(implPart);
                matcher.find(start);
            }
        }
        if (debugMode)
            System.out.println(implPart);
        else
            FileHelper.setFileData(pasFileName, intfPart + implPart);
    }

    private List<Pair<Integer, Integer>> comments;

    private void getComments(String pasFileData){
        comments = new ArrayList<>();
        Pattern p = Pattern.compile("\\{\\$IFDEF POSTGRES\\}.*?\\{\\$ENDIF\\}|\\{.*?\\}|'.*?'", Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(pasFileData);
        while (m.find()){
            comments.add(new Pair<>(m.start(), m.end()));
        }
        p = Pattern.compile("//.*");
        m = p.matcher(pasFileData);
        while (m.find()){
            comments.add(new Pair<>(m.start(), m.end()));
        }
    }

    public boolean inComment(int pos){
        return comments.stream().anyMatch(p -> {
            return (pos > p.getKey()) && (pos < p.getValue());
        });
    }

    private void addPostgresDefinesGroup(String pasFileName, List<OraDataSet> oraDataSets) {
        String pasFileData = FileHelper.getFileData(pasFileName);
        String intfPart = PasHelper.getInterfacePart(pasFileData);
        String implPart = PasHelper.getImplementationPart(pasFileData);
        getComments(implPart);
        String strPatt = "(.*?\\b(";

        for (OraDataSet ds : oraDataSets)
            strPatt += ds.name + "|";

        strPatt += "TmpDrawGeoOracleQuery|TmpGeoOracleQuery|TmpOracleQuery|OracleSession|EnergyOracleSession|" +
                "PhoneOracleSession|WaterOracleSession|GazOracleSession|HeatOracleSession|TOracleQuery|TOracleDataSet|";

        List<String> globalDataSets = iniFile.getSection("GlobalDataSets").entrySet().stream().map(e -> e.getKey()).collect(Collectors.toList());
        for (String globalDataSet : globalDataSets)
            strPatt += globalDataSet + "|";

        strPatt = strPatt.substring(0, strPatt.length() - 1) + ")\\b.*)|(.*)";
        Pattern pattern = Pattern.compile(strPatt, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(implPart);
        StringReplacer stringReplacer = new StringReplacer();
        boolean startSeq = false;
        int spaceCount = 0;
        int startPos = -1;
        int endPos = -1;
        String postgresStr = "";
        String oraStr = "", addStr = "";
        int addCount = 0;
        boolean with = false;
        int endOfWith = 0;
        String withBlock = "";
        while (matcher.find()) {
            boolean inComm = false;
            if ((matcher.group(2) != null))
                if (inComment(matcher.start(2))) inComm = true;
            if ((matcher.group(2) != null) && (!startSeq) && (!inComm)) {
                spaceCount = 0;
                startSeq = true;
                startPos = matcher.start();
            }

            if (startSeq) {
                if (matcher.group(2) == null || inComm) {
                    spaceCount++;
                    if (!"".equals(matcher.group())) {
                        addStr += matcher.group() + "\n";
                        addCount = 0;
                    } else {
                        addCount++;
                        if (addCount > 1)
                            addStr += matcher.group() + "\n";
                    }
                } else {
                    Pattern p = Pattern.compile("\\bwith\\s+" + matcher.group(2) + "\\b", Pattern.CASE_INSENSITIVE);
                    Matcher m = p.matcher(matcher.group());
                    if (m.find()){
                        with = true;
                        endOfWith = getEndOfWith(implPart, matcher.start());
                        withBlock = implPart.substring(matcher.start(), endOfWith);
                    }

                    addCount = 0;
                    spaceCount = 0;
                    if (!"".equals(addStr)) {
                        oraStr += addStr;
                        postgresStr += addStr;
                        addStr = "";
                    }
                    endPos = with ? endOfWith : matcher.end();
                    if (with) {
                        with = false;
                        oraStr += withBlock + "\n";
                        String postgr = withBlock + "\n";
                        postgr = replaceOraFuncToPostgres(postgr);
                        postgresStr += postgr;
                        matcher = pattern.matcher(implPart);
                        matcher.find(endPos);
                    } else {
                        oraStr += matcher.group() + "\n";
                        String postgr = matcher.group() + "\n";
                        postgr = replaceOraFuncToPostgres(postgr);
                        postgresStr += postgr;
                    }
                }
                if (spaceCount > 5) {
                    postgresStr = replaceDataSetsNames(postgresStr, pattern);
                    String repl =
                            "{$IFDEF POSTGRES}\n" +
                            postgresStr +
                            "{$ELSE}\n" +
                            oraStr +
                            "{$ENDIF}";
                    implPart = stringReplacer.replase(implPart, startPos, endPos, repl);
                    getComments(implPart);
                    matcher = pattern.matcher(implPart);
                    int insEndPos = startPos + repl.length();
                    matcher.find(insEndPos);
                    oraStr = "";
                    postgresStr = "";
                    startSeq = false;
                    spaceCount = 0;
                    addCount = 0;
                    addStr = "";
                }
            }
        }
        if (debugMode)
            System.out.println(implPart);
        else
            FileHelper.setFileData(pasFileName, intfPart + implPart);
    }

    private String replaceDataSetsNames(String postgresStr, Pattern pattern) {
        Matcher matcher = pattern.matcher(postgresStr);
        while (matcher.find()){
            if (matcher.group(2) != null){
                String repl = "uni_" + matcher.group(2);

                if (matcher.group(2).trim().equalsIgnoreCase("WaterOracleSession"))
                    repl = "UniConnectionWater";
                if (matcher.group(2).trim().equalsIgnoreCase("GazOracleSession"))
                    repl = "UniConnectionGaz";
                if (matcher.group(2).trim().equalsIgnoreCase("HeatOracleSession"))
                    repl = "UniConnectionHeat";

                if (matcher.group(2).trim().equalsIgnoreCase("OracleSession"))
                    repl = "UniConnection";
                if (matcher.group(2).trim().equalsIgnoreCase("EnergyOracleSession"))
                    repl = "UniConnectionEnergy";
                if (matcher.group(2).trim().equalsIgnoreCase("PhoneOracleSession"))
                    repl = "UniConnectionPhone";

                if (matcher.group(2).trim().equalsIgnoreCase("TOracleQuery"))
                    repl = "TUniQuery";
                if (matcher.group(2).trim().equalsIgnoreCase("TOracleDataSet"))
                    repl = "TUniQuery";

                if (matcher.group(2).trim().equalsIgnoreCase("TmpDrawGeoOracleQuery") ||
                        matcher.group(2).trim().equalsIgnoreCase("TmpGeoOracleQuery") ||
                        matcher.group(2).trim().equalsIgnoreCase("TmpOracleQuery"))
                    repl = "TmpUniQuery";

                postgresStr = postgresStr.replaceAll("(?i)\\b" + matcher.group(2) +
                        "\\b", repl);
                matcher = pattern.matcher(postgresStr);
            }
        }
        return postgresStr;
    }

    private int getEndOfWith(String data, int startWith){
        Pattern p = Pattern.compile("(\\b(try|begin|end)\\b(\\s*;)?)|(;)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(data);
        int findCount = 0;
        int endPos = -1;
        if (m.find(startWith))
            do {
                if (m.group(2) == null) {
                    if (m.group(4) != null && findCount == 0)
                        return m.end();
                    continue;
                }

                String find = m.group(2).toLowerCase();
                switch (find){
                    case "try" : findCount++; break;
                    case "begin" : findCount++; break;
                    case "end" : findCount--; endPos = m.end(); break;
                }
                if (findCount == 0) break;
            } while (m.find());
        if (endPos == -1) throw new RuntimeException("End of with not found");
        return endPos;
    }

    private void generateUniDataSets(String dfmFileName, String pasFileName, List<OraDataSet> oraDataSets){
        String pasFileData = FileHelper.getFileData(pasFileName);
        String dfmFileData = FileHelper.getFileData(dfmFileName);
        Map<String, String> dataSetsConformitys = iniFile.getSection("DataSets");
        StringInserter stringDfmInserter = new StringInserter(dfmFileData);
        StringInserter stringPasInserter = new StringInserter(pasFileData);
        int endPos = getEndPos(dfmFileData);
        int pasInsertPos = getPasInsertPos(pasFileData, dfmFileData);
        oraDataSets.stream().forEach(ds -> {
            StringBuilder uniDataSetCode = new StringBuilder();
            String connection = dataSetsConformitys.get(ds.session.replaceAll("\\s+", "").toLowerCase());
            if (connection == null)
                throw new RuntimeException("Connection in null");
            uniDataSetCode.append("object uni_").append(ds.name).append(": TUniQuery\n")
                    .append("\tConnection = ").append(connection).append("\n");
                    if (!"".equals(ds.sequenceField.trim()))
                        uniDataSetCode.append("\tKeyFields = '").append(ds.sequenceField).append("'\n");
                    uniDataSetCode.append("\tSQL.Strings = (").append("'{if Oracle}'\n\n" + ds.sql + "\n\n'{else}'\n\n" + ds.sql + "\n\n'{endif}'").append(")\n")
                    .append("\tLockMode = lmOptimistic\n");
            uniDataSetCode.append("\tOptions.SetFieldsReadOnly = False\n\tOptions.RequiredFields = False\n");
            if (!"".equals(ds.sequence.trim()))
                uniDataSetCode.append("\tSpecificOptions.Strings = (\n")
                        .append("\t\t'PostgreSQL.KeySequence=").append(ds.sequence).append("'\n")
                        .append("\t\t'PostgreSQL.SequenceMode=smInsert'\n")
                        .append("\t\t'Oracle.KeySequence=").append(ds.sequence).append("'\n")
                        .append("\t\t'Oracle.SequenceMode=smInsert')\n");
            uniDataSetCode.append("\tLeft = ").append(ds.left).append("\n")
                    .append("\tTop = ").append(ds.top + 28).append("\nend\n");
            stringDfmInserter.insert(endPos, uniDataSetCode.toString());
            stringPasInserter.insert(pasInsertPos, "uni_" + ds.name + ": TUniQuery;\n\t");
        });
        stringDfmInserter.flush();
        stringPasInserter.flush();
        if (debugMode) {
            System.out.println(stringPasInserter.getResult());
            System.out.println("-------------------------------------------------------");
            System.out.println(stringDfmInserter.getResult());
        } else {
            FileHelper.setFileData(pasFileName, stringPasInserter.getResult());
            FileHelper.setFileData(dfmFileName, stringDfmInserter.getResult());
        }
    }

    private int getPasInsertPos(String pasFileData, String dfmFileData) {
        String strPat = "\\b(object|inherited)\\s+(\\w+)\\s*:\\s*(\\w+)\\b";
        Pattern pattern = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(dfmFileData);
        String className = "";
        if (matcher.find())
            className = matcher.group(3);
        if ("".equals(className.trim())) throw new RuntimeException("Class name is empty");
        pattern = Pattern.compile("\\b" + className + "\\s*=\\s*class\\s*\\(\\w+\\)\\s*", Pattern.CASE_INSENSITIVE);
        matcher = pattern.matcher(pasFileData);
        if (matcher.find()) {
            this.className = className;
            return matcher.end();
        }
        throw new RuntimeException("Class not found");
    }

    private int getEndPos(String dfmFileData) {
        Pattern pattern = Pattern.compile("\\bend\\b", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(dfmFileData);
        ReverseMatcher reverseMatcher = new ReverseMatcher(matcher);
        if (reverseMatcher.find())
            return reverseMatcher.start();
        throw new RuntimeException("End of file not found");
    }

    private List<OraDataSet> findOraDataSets(String dfmFileName){
        String dfmFileData = FileHelper.getFileData(dfmFileName);
        List<OraDataSet> dataSets = new ArrayList<>();
        Pattern pattern = Pattern.compile("\\bobject\\s+([a-z0-9_]+)\\s*:\\s*TOracleDataSet\\b", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(dfmFileData);
        while (matcher.find()){
            OraDataSet dataSet = new OraDataSet();
            dataSet.name = matcher.group(1);

            if (someDataSets)
                if (!this.dataSets.stream().anyMatch(s -> s.equalsIgnoreCase(dataSet.name)))
                    continue;

            String dataSetCode = PasHelper.getObjectCode(dfmFileData, dataSet.name);
            dataSet.sql = getSqlFromDataSet(dataSetCode);
            dataSet.session = getData(dataSetCode, "Session");
            try {
                dataSet.left = Integer.valueOf(getData(dataSetCode, "Left"));
            } catch (Exception e){
                dataSet.left = 1;
            }
            try {
                dataSet.top = Integer.valueOf(getData(dataSetCode, "Top"));
            } catch (Exception e){
                dataSet.top = 1;
            }

            dataSet.sequenceField = PasHelper.getDataInBounds(dataSetCode, "SequenceField", "Field", '\'', '\'');
            dataSet.sequence = PasHelper.getDataInBounds(dataSetCode, "SequenceField", "Sequence", '\'', '\'');
            Map<String, String> dataSources = getDataSources(dfmFileData);
            dataSet.dataSource = dataSources.getOrDefault(dataSet.name.toLowerCase(), "");
            dataSet.validate();
            dataSets.add(dataSet);
        }
        return dataSets;
    }

    private Map<String, String> getDataSources(String dfmFileData){
        Map<String, String> result = new HashMap<>();
        Pattern pattern = Pattern.compile("\\bobject\\s+([a-z0-9_]+)\\s*:\\s*TDataSource\\b", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(dfmFileData);
        while (matcher.find()){
            String dataSourceName = matcher.group(1);
            String code = PasHelper.getObjectCode(dfmFileData, dataSourceName);
            Pattern p = Pattern.compile("\\bDataSet\\s*=\\s*(\\w+)\\b", Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(code);
            if (m.find()) {
                String dataSetName = m.group(1);
                result.put(dataSetName.toLowerCase(), dataSourceName);
            } else {
                throw new RuntimeException("DataSet not found");
            }
        }
        return result;
    }

    class OraDataSet {
        public String name;
        public String sql;
        public String session;
        public String sequence;
        public String sequenceField;
        public String dataSource;
        public int left = 1, top = 1;

        public void validate() {
            if ("".equals(name.trim())) throw new RuntimeException("Name is empty");
            if ("".equals(sql.trim())) throw new RuntimeException("Sql is empty");
            if ("".equals(session.trim())) throw new RuntimeException("Session is empty");
        }
    }

    public static String getData(String data, String name) {
        String strPat = "\\b" + name + "\\b\\s*=\\s*\\b(.+)\\b";
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(data);
        if (!m.find()) return "";
        return m.group(1);
    }

    private String getSqlFromDataSet(String dataSetCode){
        String sql;
        String strPat = "\\bSQL\\b\\s*\\.\\s*\\bStrings\\b\\s*=";
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(dataSetCode);
        if (!m.find()) return "'!'";
        sql = PasHelper.getInBrackets(dataSetCode.substring(m.end()));
        return sql;
    }
}
