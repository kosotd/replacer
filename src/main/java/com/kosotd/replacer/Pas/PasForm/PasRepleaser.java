package com.kosotd.replacer.Pas.PasForm;

import com.kosotd.replacer.RegexHelpers.ReverseMatcher;
import com.kosotd.replacer.StringHelpers.StringInserter;
import com.kosotd.replacer.StringHelpers.StringReplacer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasRepleaser {  
    
    boolean replase;
    List<String> varNames = new ArrayList<>();
    private boolean ignoreWordWithFPrefix = false;
    
    public PasRepleaser(boolean ignoreWordWithFPrefix){
        this.ignoreWordWithFPrefix = ignoreWordWithFPrefix;
    }
    
    public void setVarNames(List<String> varNames){
        this.varNames = varNames;
    }
    
    public boolean isReplase(){
        return replase;
    }
    
    private int getEndModulePos(String fileData){
        int res = fileData.length();
        Pattern findEnd = Pattern.compile("\\bend\\b", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Pattern findInit = Pattern.compile("\\binitialization\\b", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher endMatch = findEnd.matcher(fileData);
        Matcher initMatch = findInit.matcher(fileData);
        ReverseMatcher rm = new ReverseMatcher(endMatch);
        ReverseMatcher rim = new ReverseMatcher(initMatch);
        if (rim.find())
            res = rim.start();
        else if (rm.find())                
            res = rm.start();
        return res;
    }
    
    private int getPrivateDecPos(String fileData, int start){
        int res = fileData.length();
        Pattern findDec = Pattern.compile("\\b(private|protected|public)\\b", 
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher decMatch = findDec.matcher(fileData);
        if (decMatch.find(start))
            res = decMatch.end();
        return res;
    }
    
    public String replease(String className, String replaseClassName, String fileData){
        
        VariableFinder vf = new VariableFinder(className, fileData);
        StringInserter si = new StringInserter(fileData);
        Map<String, String> relpace = new HashMap<>();
        replase = false;
        
        for (VarEntry ve : vf){   
            
            if (ignoreWordWithFPrefix)
                if ((ve.getVarName().charAt(0) == 'F') || (ve.getVarName().charAt(0) == 'f'))
                    continue;
            
            boolean find = false;
            for (String s : varNames){
                if (s.toLowerCase().equals(ve.getVarName().toLowerCase())){
                    find = true;
                    break;
                }
            }
            if ((!find) || (ve.getVarPos() > ve.getClassEndPos())) {
                StringReplacer sr = new StringReplacer(StringReplacer.WHOLE_WORD | StringReplacer.CASE_INSENSITIVE);
                relpace.put(ve.getStatement(), sr.replase(ve.getStatement(), className, replaseClassName));
                continue;
            }

            replase = true;
            String insStr = "F";
            si.insert(ve.getVarPos(), insStr);
            
            insStr = "\n\tFI" + ve.getVarName() + " : " + replaseClassName + ";";            
            si.insert(getPrivateDecPos(fileData, ve.getVarPos()), insStr);          

            insStr = "\tfunction Get" + ve.getVarName() + "() : " + replaseClassName + ";\n";
            insStr += "\tproperty " + ve.getVarName() + ": " + replaseClassName + " read Get" + 
                ve.getVarName() + ";\n";            
            si.insert(ve.getClassEndPos(), insStr);
            
            insStr = "function " + ve.getClassName() + ".Get" + ve.getVarName() + "() : " + replaseClassName + ";\n";
            String fMethodName = replaseClassName.substring(1);
            insStr += "begin\n" +
                    "\tif (FI" + ve.getVarName() + " = nil) then\n"  +
                    "\t\tFI" + ve.getVarName() + " := GetDBItemsFactory.Create" + fMethodName + "FromExisting(F" + ve.getVarName() + ");\n" +
                    "\tresult := FI" + ve.getVarName() + ";\n" + 
                    "end;\n\n";
            si.insert(getEndModulePos(fileData), insStr);
        }
        si.flush();
        String res = si.getResult();
        StringReplacer sr = new StringReplacer(StringReplacer.WHOLE_WORD | StringReplacer.CASE_INSENSITIVE);
        for (Entry<String, String> e : relpace.entrySet())
            res = sr.replase(res, e.getKey(), e.getValue());
        return res;
    }
  
}
