/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 *
 * @author User
 */
@XmlRootElement
public class Guide {
    public String sql;
    
        @XmlAttribute
        public String name;
        public String caption;
        public String sequenceField;
        public String sequence;
        public String isLevelsGuide;
        public String parentField;
        public String keyField;
        public String mustSelect;
        public String fieldId;
        public String fieldText;
        public String extSchema;
        
        public Columns columns;
        
        public Guide(){
            columns = new Columns();
        }
        
        public void validateFields(){
            if (columns.columns.size() < 1)
                throw new IllegalArgumentException("columns is empty");
            for (Column col : columns.columns)
                col.validateFields();
            if ("".equals(sql.trim()) || "".equals(sequenceField.trim()) || "".equals(sequence.trim()))
                throw new IllegalArgumentException("field is empty");
            sequence = sequence.trim();
            sequenceField = sequenceField.trim();
        }
        
        @Override
        public String toString(){
            String res = "";
            res += "Sql : " + sql + "\n";
            res += "sequence : " + sequence + "\n";
            res += "sequenceField : " + sequenceField + "\n";
            res += "columns :\n" + columns + "\n";
            return res;
        }
}
