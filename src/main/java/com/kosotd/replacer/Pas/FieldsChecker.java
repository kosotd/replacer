/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas;

import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.Pas.PasHelpers.PasHelper;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author User
 */
public class FieldsChecker {
    public static void main(String[] args) throws IOException{
        FieldsChecker fieldsChecker = new FieldsChecker();
        String dir = "C:\\Users\\User\\Desktop\\UrbaniCS\\src";
        
        Files.walk(Paths.get(dir))
        .filter(s->s.toAbsolutePath().toString().toLowerCase().endsWith(".dfm"))
        .forEach(path -> fieldsChecker.checkFields(path.toString()));
    }

    private void checkFields(String dfmFileName) {
        String dfmFileData = FileHelper.getFileData(dfmFileName);
        Pattern pattern = Pattern.compile("\\bobject\\s+([a-z0-9_]+)\\s*:\\s*TOracleDataSet\\b", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(dfmFileData);
        while (matcher.find()){
            if (matcher.group(1).toLowerCase().startsWith("ftmp") || 
                "fordeletedataset".equals(matcher.group(1).toLowerCase()))
                continue;
            String code = PasHelper.getObjectCode(dfmFileData, matcher.group(1));
            String uniqueFields = getData(code, "UniqueFields");
            String sequenceField = getData(code, "SequenceField\\.Field");
            if ("".equals(uniqueFields.trim()) && "".equals(sequenceField.trim())){
                File file = new File(dfmFileName);
                System.out.println(file.getName() + " -> " + matcher.group(1));
            }            
        }
    }
    
    private String getData(String data, String name) {
        String strPat = "\\b" + name + "\\b\\s*=(.*)";
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(data);
        if (!m.find()) return "";
        return m.group(1);
    }
}
