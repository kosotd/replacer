/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas;

import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.FileHelpers.JIniFile;
import com.kosotd.replacer.Pas.PasHelpers.PasHelper;
import com.kosotd.replacer.StringHelpers.StringReplacer;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javafx.util.Pair;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class XMLBuilder {
    
    private Guides guides = new Guides();
    private Map<String, File> dataModulesFiles;
    private Map<String, String> dataModules;
    private String xmlFileName;
    private JIniFile iniFile;
    
    public XMLBuilder(String iniFileName, String xmlFileName, Map<String, File> dataModulesFiles){
        if (dataModulesFiles == null) return;
        iniFile = new JIniFile(iniFileName);
        dataModules = iniFile.getSection("DataModules");
        this.dataModulesFiles = dataModulesFiles;
        this.xmlFileName = xmlFileName;
    }
    
    public void accumulate(String guideFileName){
        String guideFileData = FileHelper.getFileData(guideFileName + ".dfm");
        Pair<String, String> pair = getDataModuleFile(guideFileData);        
        String dataModuleFile = dataModulesFiles.get(dataModules.get(pair.getKey()).toLowerCase().trim()).getAbsolutePath();
        String dataModuleFileData = FileHelper.getFileData(dataModuleFile);
        String dataSetName = getDataSetName(dataModuleFileData, pair.getValue());
        String dataSetCode = PasHelper.getObjectCode(dataModuleFileData, dataSetName);
        String gridViewCode = PasHelper.getObjectCode(guideFileData, "(GuideGridView|GuideList)");
        Guide guide = getResult(dataSetCode, dataSetName, gridViewCode);
        guide.caption = getGuideCaption(guideFileData);
        String fileData = FileHelper.getFileData(guideFileName + ".pas");
        guide.name = PasHelper.getModuleName(fileData);
        store(guide.name, fileData);
        guide.mustSelect = checkMustSelect(fileData);
        if (guide.mustSelect.equals("True"))
            getSelectFields(fileData, guide);
        guide.extSchema = getExtSchema(fileData);
        guide.validateFields();     
        guides.guides.add(guide);
    }
    
    private void store(String unitName, String pasFileData){
        String strPat = "\\b([0-9a-z_]+)\\s*=\\s*class\\s*\\(.*?\\)";
        Pattern pattern = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(pasFileData);
        if (matcher.find()){
            String className = matcher.group(1);
            iniFile.writeString("GuidesClassNames", className, unitName);
            iniFile.updateFile();
            return;
        }
        throw new AssertionError();
    }
    
    private String getGuideCaption(String dfmFileData){
        String caption = getCaption(dfmFileData);
        String decodeCaption = decodeCaption(caption);
        if (decodeCaption.trim().equals("")) 
            throw new AssertionError();
        return decodeCaption;
    }
    
    private void getSelectFields(String fileData, Guide guide) {
        Pattern p = Pattern.compile("\\bfunction\\s+[a-z0-9_]+\\s*\\.\\s*GetSelectData\\b", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(fileData);
        if (!m.find()) 
            throw new AssertionError();
        
        p = Pattern.compile("\\bID\\s*:=\\s*FieldByName\\s*\\(\\s*'([a-z0-9_]+)'", Pattern.CASE_INSENSITIVE);
        Matcher m1 = p.matcher(fileData);
        if (!m1.find(m.start())) 
            throw new AssertionError();
        guide.fieldId = m1.group(1);
        
        p = Pattern.compile("\\bText\\s*:=\\s*FieldByName\\s*\\(\\s*'([a-z0-9_]+)'", Pattern.CASE_INSENSITIVE);
        m1 = p.matcher(fileData);
        if (!m1.find(m.start()))
            throw new AssertionError();
        guide.fieldText = m1.group(1);
    }
    
    private String getExtSchema(String fileData) {
        Pattern p = Pattern.compile("\\bCheckExtSchemaConnection\\s*\\(\\s*([a-z]+)\\s*\\)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(fileData);
        if (m.find())
            return m.group(1);
        else
            return "";
    }
    
    private String checkMustSelect(String fileData) {
        Pattern p = Pattern.compile("\\bGetSelectData\\b", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(fileData);
        if (m.find())
            return "True";
        else
            return "False";
    }
    
    public void storeToXml(){
        try {
            File file = new File(xmlFileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(Guides.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(guides, file);
        } catch (JAXBException e) {
        }
    }
    
    private String getDataSetName(String dataModuleFileData, String value) {
        int pos = getPos(dataModuleFileData, "object", value, "TDataSource");
        String strPat = "\\bDataSet\\b\\s*=\\s*\\b([0-9a-z_]+)\\b";
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(dataModuleFileData);
        if (!m.find(pos)) return "";
        return m.group(1);
    }
    
    private Guide getResult(String dataSetCode, String value, String gridViewCode) {
        Guide guide = new Guide();        
        String strPat = "\\bSQL\\b\\s*\\.\\s*\\bStrings\\b\\s*=";
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(dataSetCode);
        if (!m.find()) return null;
        guide.sql = PasHelper.getInBrackets(dataSetCode.substring(m.end()));
        guide.sql = guide.sql.replaceAll("\n", "");
        guide.sql = guide.sql.replaceAll("'\\s*\\+\\s*'", ""); 
        guide.sql = guide.sql.replaceAll("'", "");
        guide.sql = guide.sql.replaceAll("\\s+", " ");
        guide.sql = guide.sql.trim();
        guide.sql = PasHelper.replaceUnicode(guide.sql);
        guide.sequenceField = PasHelper.getDataInBounds(dataSetCode, "SequenceField", "Field", '\'', '\'');
        guide.sequence = PasHelper.getDataInBounds(dataSetCode, "SequenceField", "Sequence", '\'', '\'');
        strPat = "\\bobject\\b\\s*\\b[0-9a-z_]*[0-9a-z_]+\\b\\s*:\\s*\\b(TcxGridDBColumn|TcxDBTreeListColumn)\\b.*?\\bend\\b";
        p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        m = p.matcher(gridViewCode);
        guide.isLevelsGuide = "False";
        while (m.find()){            
            String field = m.group();
            String type = m.group(1).toLowerCase();
            Column col = new Column();
            if (type.equals("tcxdbtreelistcolumn")){
                guide.isLevelsGuide = "True";
                col.positionBandIndex = getData(field, "Position\\.BandIndex");
                col.positionColIndex = getData(field, "Position\\.ColIndex");
                col.positionRowIndex = getData(field, "Position\\.RowIndex");
            }
            col.fieldName = PasHelper.getDataInBounds(field, "DataBinding", "FieldName", '\'', '\'');
            col.visible = getData(field, "Visible");
            col.width = getData(field, "Width");
            col.caption = getCaption(field);
            col.caption = decodeCaption(col.caption);
            col = getFieldTypeAndRequiredCaption(col.fieldName, dataSetCode, col);
            guide.columns.columns.add(col);
        } 
        if (guide.isLevelsGuide.equals("True")){
            guide.parentField = PasHelper.getDataInBounds(gridViewCode, "DataController", "ParentField", '\'', '\'');
            guide.keyField = PasHelper.getDataInBounds(gridViewCode, "DataController", "KeyField", '\'', '\'');
        }       
        return guide;
    }
    
    public String getCaption(String field){
        String strPat = "\\b(Caption(\\.Text)?\\b\\s*=)\\s*(.*)\\s*";
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(field);
        String caption = "";
        if (m.find()) caption = m.group(3);
        strPat = "\\s*\\+\\s*$";
        p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher m1 = p.matcher(caption);
        if (m1.find()){
            String rep = m1.replaceAll("");
            StringReplacer sr = new StringReplacer();
            field = sr.replase(field, m.start(), m.end(), m.group(1) + " " + rep);
            return getCaption(field);
        }        
        return caption;
    }

    public String decodeCaption(String caption){
        Pattern p = Pattern.compile("'(.*?)'");
        Matcher m = p.matcher(caption);

        int start = -1, end = -1;
        if (m.find()) {
            start = m.start();
            end = m.end();
        }
        String result = "";
        if (start == -1) {
            String[] strs = caption.trim().split("#");
            result = Arrays.stream(strs).filter(x -> !"".equals(x.trim())).reduce("",
                    (s1, s2) -> (s1 += (char) Integer.parseInt(s2))).trim();
        } else {
            String[] strs = caption.substring(0, start).trim().split("#");
            result = Arrays.stream(strs).filter(x -> !"".equals(x.trim())).reduce("",
                    (s1, s2) -> (s1 += (char) Integer.parseInt(s2))).trim() + m.group(1) +
                    decodeCaption(caption.substring(end));
        }
        return result;
    }
    
    Map<String, String> exceptions = new HashMap<String, String>(){{
        put("SEWAGE_TYPE_ID", "SPT_ID");
        put("WATER_PIPE_TYPE_ID", "WPT_ID");
    }};
    
    private Column getFieldTypeAndRequiredCaption(String fieldName, String dataSetCode, Column col){       
        String strPat = "\\bobject\\b\\s*\\b[0-9a-z_]*" + fieldName + "\\b\\s*:\\s*\\bT([a-z]+)Field\\b.*?\\bend\\b";
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher m = p.matcher(dataSetCode);
        if (m.find()){            
            String field = m.group();
            col.valueType = m.group(1);
            col.required = getData(field, "Required");
        } else {
            if (exceptions.containsKey(fieldName))
                return getFieldTypeAndRequiredCaption(exceptions.get(fieldName), dataSetCode, col);
        }
        return col;
    }
    
    private Pair<String, String> getDataModuleFile(String guideFileData){
        int pos = getPos(guideFileData, "inherited", "GuideGridView", "TcxGridDBTableView");
        if (pos == guideFileData.length())
            pos = getPos(guideFileData, "inherited", "GuideList", "TcxDBTreeList");
        String strPat = "\\bDataController\\b\\s*\\.\\s*\\bDataSource\\b\\s*=\\s*\\b([0-9a-z_]+)\\b\\s*\\.\\s*\\b([0-9a-z_]+)\\b";
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(guideFileData);
        if (!m.find(pos)) return new Pair<>("", "");
        return new Pair<>(m.group(1), m.group(2));
    } 
   
    private int getPos(String data, String type1, String name, String type2){
        String strPat = "\\b" + type1 + "\\b\\s*\\b" + name + "\\b\\s*:\\s*\\b" + type2 + "\\b";
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(data);
        if (!m.find()) return data.length();
        return m.end();
    }

    private String getData(String data, String name) {
        String strPat = "\\b" + name + "\\b\\s*=\\s*\\b([0-9a-z_]+)\\b";
        Pattern p = Pattern.compile(strPat, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(data);
        if (!m.find()) return "";
        return m.group(1);
    }
      
}
