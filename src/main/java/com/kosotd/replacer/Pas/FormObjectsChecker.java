/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas;

import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.Pas.PasHelpers.PasHelper;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author User
 */
public class FormObjectsChecker {
    public static void main(String[] args) throws IOException{
        FormObjectsChecker objectsChecker = new FormObjectsChecker();
        String dir = "C:\\Users\\User\\Desktop\\UrbaniCS\\src";
        
        //dir = "C:\\Users\\User\\Desktop\\replacer\\files";
        //dir = "D:\\Src\\Projects\\UrbaniCS\\2.5\\source";
        
        Files.walk(Paths.get(dir))
        .filter(s->s.toAbsolutePath().toString().toLowerCase().endsWith(".dfm"))
        .forEach(path -> objectsChecker.checkObjects(path.toString()));
    }
    
    private void checkObjects(String dfmFileName){
        String pasFileName = FileHelper.getFileNameWithoutExt(dfmFileName) + ".pas";
        File file = new File(pasFileName);
        String fileName = file.getName();
        String pasFileData = FileHelper.getFileData(pasFileName);
        String dfmFileData = FileHelper.getFileData(dfmFileName);
        Pattern pattern = Pattern.compile("\\b(object|inherited)\\s+(\\w+)\\s*:\\s*(\\w+)\\b", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(dfmFileData);
        while (matcher.find()){
            if (matcher.start() == 0) continue;
            String objKind = matcher.group(1);
            boolean find = true;
            if ("object".equals(objKind.toLowerCase()))
                find = false;
            String intfPart = PasHelper.getInterfacePart(pasFileData);
            Pattern pasPattern = Pattern.compile("\\b(" + matcher.group(2) + 
                    ")\\s*:\\s*(" + matcher.group(3) + ")\\b", Pattern.CASE_INSENSITIVE);
            Matcher pasMatcher = pasPattern.matcher(intfPart);
            if (pasMatcher.find() == find){
                System.out.println(fileName + " -> " + matcher.group(2) + ": " + 
                        matcher.group(3) + " " + objKind.toLowerCase());
            }
        }
    }
}
