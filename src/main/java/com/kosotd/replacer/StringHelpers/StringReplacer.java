package com.kosotd.replacer.StringHelpers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringReplacer {
    public static final int NONE = 1;
    public static final int WHOLE_WORD = 2; 
    public static final int REGEX_FIND = 4; 
    public static final int CASE_INSENSITIVE = 8; 
    
    public StringReplacer(){
    }
    
    public StringReplacer(int fileReplacerFlag){
        this.fileReplacerFlag = fileReplacerFlag;
    }

    public String replase(String lines, String thatReplaced, String youReplace){
        if (!checkFlag(REGEX_FIND))
            thatReplaced = replaceSpecialSymbols(thatReplaced);
        if (checkFlag(WHOLE_WORD)){
            thatReplaced = isWordSymbol(thatReplaced.charAt(0)) ? "\\b" + thatReplaced : thatReplaced;
            thatReplaced = isWordSymbol(thatReplaced.charAt(thatReplaced.length() - 1)) ? thatReplaced + "\\b" : thatReplaced;
        }
        try {
            int flags = checkFlag(CASE_INSENSITIVE) ? Pattern.MULTILINE | 
                Pattern.CASE_INSENSITIVE : Pattern.MULTILINE;
            Pattern p = Pattern.compile(thatReplaced, flags);
            Matcher m = p.matcher(lines);
            lines = m.replaceAll(youReplace);
        } catch (Exception e){
        }
        return lines;
    }

    public String replase(String lines, int start, int end, String yourReplace){
        StringBuilder sb = new StringBuilder(lines);
        sb = sb.replace(start, end, yourReplace);
        return sb.toString();
}
    
    public int getFileReplacerFlag(){
        return fileReplacerFlag;
    }
    
    public void setFileReplacerFlag(int fileReplacerFlag){
        this.fileReplacerFlag = fileReplacerFlag;
    }
    
    public void addFileReplacerFlag(int fileReplacerFlag){
        this.fileReplacerFlag |= fileReplacerFlag;
    }
    
    private boolean isWordSymbol(Character c){
        return Pattern.compile("\\w|[а-я]|[А-Я]").matcher(c.toString()).matches();
    }
    
    private String replaceSpecialSymbols(String s){
        for (String sym : specialSymbols){
            if (sym.equals("\\\\")) 
                s = s.replaceAll(sym, "\\\\\\\\");
            else if (sym.equals("\\$"))
                s = s.replaceAll(sym, "\\\\\\$");
            else
                s = s.replaceAll(sym, "\\" + sym);
        }
        return s;
    }
    
    private boolean checkFlag(int flag){
        return (fileReplacerFlag & flag) != 0;
    }
    
    private int fileReplacerFlag = NONE;
    
    private final List<String> specialSymbols = new ArrayList<String>(){{
        add("\\\\"); add("\\$");
        add("\\("); add("\\)"); add("\\."); add("\\*"); add("\\^");
        add("\\|"); add("\\?"); add("\\+"); add("\\["); add("\\]"); add("\\,"); 
        add("\\{"); add("\\}");  add("\\-"); 
    }};
}
