package com.kosotd.replacer.StringHelpers;

import java.util.ArrayDeque;
import java.util.Queue;
import javafx.util.Pair;

public class StringInserter {

    public StringInserter(String data){
        this.data = data;
    }

    public void insert(int pos, String str){
        strings.offer(new InsPair(pos, str));
    }

    public void flush(){
        while (strings.size() > 0){
            InsPair pair = strings.poll();
            for (InsPair p : strings)
                if (pair.getKey() < p.getKey())
                    p.setKey(p.getKey() + pair.getValue().length());
            data = new StringBuilder(data).insert(pair.getKey(), pair.getValue()).toString(); 
        }
    }

    public String getResult(){
        return data;
    }

    private class InsPair {
        private Pair<Integer, String> pair;

        public void setKey(Integer key){ pair = new Pair<>(key, pair.getValue()); }            
        public void setValue(String value){ pair = new Pair<>(pair.getKey(), value); }
        public Integer getKey(){ return pair.getKey(); }            
        public String getValue(){ return pair.getValue(); }
        public InsPair(Integer k, String v) { pair = new Pair<>(k, v); }
    }
    
    private String data;
    private final Queue<InsPair> strings = new ArrayDeque<>();
}
