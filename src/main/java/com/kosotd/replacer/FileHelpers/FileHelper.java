package com.kosotd.replacer.FileHelpers;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class FileHelper {
    
    public static String getFileNameWithoutExt(File file){
        return getFileNameWithoutExt(file.toString());
    }
    
    public static String getFileNameWithoutExt(String fileName){
        return fileName.replaceFirst("[.][^.]+$", "");
    }
    
    public static String getExtension(File file){
        return getExtension(file.toString());
    }
    
    public static String getExtension(String fileName){
        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            return fileName.substring(i+1);
        }
        return "";
    }
    
    public static String getFileData(File file){
        return getFileData(file.toString());
    }
    
    public static String getFileData(String fileName){
        try {
            FileInputStream fis = new FileInputStream(fileName);
            InputStreamReader isr = new InputStreamReader(fis, "Cp1251");
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            br.lines().forEach((s)->{
                sb.append(s);
                sb.append("\n");
            });
            return sb.toString();
        } catch (Exception ex) {
        }
        return "";
    }

    public static void copyFiles(String srcDir, String destDir) throws IOException {
        Files.walk(Paths.get(srcDir))
                .filter(s -> new File(String.valueOf(s)).isFile())
                .forEach(path -> {
                    String s = path.toAbsolutePath().toString();
                    int oldLen = s.length();
                    s = s.replace(srcDir, "");
                    if (oldLen == s.length())
                        throw new RuntimeException("Uncorrect path replace");
                    try {
                        Path dir = Paths.get(destDir.toString() + "\\" + s).getParent();
                        if (Files.notExists(dir)) {
                            Files.createDirectory(dir);
                        }
                        Files.copy(path, Paths.get( destDir.toString() + "\\" + s), StandardCopyOption.REPLACE_EXISTING);
                    } catch (IOException e) {
                        throw new RuntimeException("Could not create file");
                    }
                });
    }
    
    public static void setFileData(File file, String data){
        setFileData(file.toString(), data);
    }
    
    public static void setFileData(String fileName, String data){
        try {
            FileOutputStream fis = new FileOutputStream(fileName);
            OutputStreamWriter isr = new OutputStreamWriter(fis, "Cp1251");
            BufferedWriter br = new BufferedWriter(isr);
            br.write(data);
            br.flush();
            br.close();
        } catch (Exception ex) {
        }
    }
}
