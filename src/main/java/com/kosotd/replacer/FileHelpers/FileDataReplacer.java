package com.kosotd.replacer.FileHelpers;

import com.kosotd.replacer.StringHelpers.StringReplacer;
import java.io.File;

public class FileDataReplacer {
    
    public FileDataReplacer(){
        stringReplacer = new StringReplacer();
    }
    
    public FileDataReplacer(int fileReplacerFlag){
        stringReplacer = new StringReplacer(fileReplacerFlag);
    }
    
    public void replase(File file, String thatReplaced, String youReplace){
        replase(file.toString(), thatReplaced, youReplace);
    }
    
    public void replase(String fileName, String thatReplaced, String youReplace){
        String fileData = FileHelper.getFileData(fileName);
        String newData = stringReplacer.replase(fileData, thatReplaced, youReplace);
        FileHelper.setFileData(fileName, newData);
    }
    
    public int getFileReplacerFlag(){
        return stringReplacer.getFileReplacerFlag();
    }
    
    public void setFileReplacerFlag(int fileReplacerFlag){
        stringReplacer.setFileReplacerFlag(fileReplacerFlag);
    }
    
    private final StringReplacer stringReplacer;
}
