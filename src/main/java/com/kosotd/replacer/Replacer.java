package com.kosotd.replacer;

import com.kosotd.replacer.FileHelpers.FileDataReplacer;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import static java.nio.file.FileVisitResult.CONTINUE;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.FileHelpers.JIniFile;
import com.kosotd.replacer.Pas.PasForm.BaseClassReplacer;
import com.kosotd.replacer.Pas.PasForm.PasFormReplacer;
import com.kosotd.replacer.Pas.PasForm.PropertiesEditor;
import com.kosotd.replacer.Pas.PasForm.VisualComponentsChanger;
import com.kosotd.replacer.Pas.PasHelpers.PasHelper;
import com.kosotd.replacer.Pas.PasUsesEditor;
import com.kosotd.replacer.StringHelpers.StringReplacer;
import com.kosotd.replacer.UI.MainForm;
import com.kosotd.replacer.UI.ProgressBarProcess;


public class Replacer {
    
    public Replacer(String iniFileName, String rootFolder, String projectFile, 
        boolean ignoreWordsWithFPrefix, boolean onlyInPrjFiles, 
            ProgressBarProcess progressBarProcess){
        this.progressBarProcess = progressBarProcess;
        this.projectFile = projectFile;
        this.iniFileName = iniFileName;
        this.rootFolder = rootFolder;
        this.onlyInPrjFiles = onlyInPrjFiles;
        this.ignoreWordsWithFPrefix = ignoreWordsWithFPrefix;
    }
    
    public void ReplaceDfmVars() {
        try {
            CountFilesVisitor vis = new CountFilesVisitor(new String[]{"dfm"});
            Files.walkFileTree(Paths.get(rootFolder), vis);
            progressBarProcess.init(vis.getCount());
            Files.walkFileTree(Paths.get(rootFolder), new ReplaceDfmVarsVisitor("dfm"));
        } catch (IOException ex) {
        }
    }
    
    public void ReplaceBaseClass() {
        try {
            CountFilesVisitor vis = new CountFilesVisitor(new String[]{"dfm"});
            Files.walkFileTree(Paths.get(rootFolder), vis);
            progressBarProcess.init(vis.getCount());
            Files.walkFileTree(Paths.get(rootFolder), new ReplaceBaseClassVisitor("dfm"));
        } catch (IOException ex) {
        }
    }

    public void ReplaceProperties() {
        try {
            CountFilesVisitor vis = new CountFilesVisitor(new String[]{"pas"});
            Files.walkFileTree(Paths.get(rootFolder), vis);
            progressBarProcess.init(vis.getCount());
            Files.walkFileTree(Paths.get(rootFolder), new ReplacePropertiesVisitor("pas"));
        } catch (IOException ex) {
        }
    }
    
    public void StoreTypesAndFuncsToIniFile() {
        try {
            CountFilesVisitor vis = new CountFilesVisitor(new String[]{"pas"});
            Files.walkFileTree(Paths.get(rootFolder), vis);
            progressBarProcess.init(vis.getCount());
            StoreTypesAndFuncsVisitor visitor = new StoreTypesAndFuncsVisitor("pas");
            Files.walkFileTree(Paths.get(rootFolder), visitor);
            visitor.saveIniFile();
        } catch (IOException ex) {
        }
    }
    
    public void InsertUses() {
        try {
            CountFilesVisitor vis = new CountFilesVisitor(new String[]{"pas"});
            Files.walkFileTree(Paths.get(rootFolder), vis);
            progressBarProcess.init(vis.getCount());
            Files.walkFileTree(Paths.get(rootFolder), new InsertUsesFileVisitor("pas"));
        } catch (IOException ex) {
        }
    }
    
    public void ReplaceFormVars() {
        try {
            CountFilesVisitor vis = new CountFilesVisitor(new String[]{"pas", "dfm"});
            Files.walkFileTree(Paths.get(rootFolder), vis);
            progressBarProcess.init(vis.getCount());
            iter = 0;
            Files.walkFileTree(Paths.get(rootFolder), new ReplaceFormVarsFileVisitor("dfm"));
            Files.walkFileTree(Paths.get(rootFolder), new ReplaceFormVarsFileVisitor("pas"));
        } catch (IOException ex) {
        }
    }
    
    public void ReplaceModuleVars() {
        try {
            CountFilesVisitor vis = new CountFilesVisitor(new String[]{"pas"});
            Files.walkFileTree(Paths.get(rootFolder), vis);
            progressBarProcess.init(vis.getCount());
            Files.walkFileTree(Paths.get(rootFolder), new ReplaceModuleVarsVisitor("pas"));
        } catch (IOException ex) {
        }
    }
    
    private boolean isFileInProject(Path file) {
        String fileNameWithOutExt = FileHelper.getFileNameWithoutExt(file.getFileName().toString());
        Pattern p = Pattern.compile("\\b" + fileNameWithOutExt + "\\b\\s*in\\s*'.*?'", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Matcher m = p.matcher(FileHelper.getFileData(projectFile));
        return m.find();
    }

    public void ChangeButtonsStyle() {
        try {
            CountFilesVisitor vis = new CountFilesVisitor(new String[]{"dfm"});
            Files.walkFileTree(Paths.get(rootFolder), vis);
            progressBarProcess.init(vis.getCount());
            Files.walkFileTree(Paths.get(rootFolder), new ChangeButtonsStyle("dfm"));
        } catch (IOException ex) {
        }
    }

    private class StoreTypesAndFuncsVisitor extends SimpleFileVisitor<Path>{

        public StoreTypesAndFuncsVisitor(String ext){
            this.extension = ext.toLowerCase();
            iniFile = new JIniFile(iniFileName);
            iter = 0;
        }
        
        public void saveIniFile(){
            iniFile.updateFile();
        }
        
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
            String currExtension = FileHelper.getExtension(file.getFileName().toString()).toLowerCase();
            if (onlyInPrjFiles)
                if (!isFileInProject(file)) return CONTINUE;
            if (attr.isRegularFile() && currExtension.equals(extension)) {
                String fileData = FileHelper.getFileData(file.toString());
                List<String> typesAndFuncs = PasHelper.getDefinedTypesAndFuncs(fileData);
                String moduleName = PasHelper.getModuleName(fileData);
                typesAndFuncs.stream().forEach(s -> {
                    if (iniFile.valueExist("Names", s)){
                        System.err.print("Duplicate value: " + s + " in " + iniFile.readString("Names", s, ""));
                        System.err.println(" and " + moduleName);
                    } 
                    iniFile.writeString("Names", s, moduleName);
                });
                progressBarProcess.process(++iter);
                MainForm.writelnMsg("Stored from " + file.toString());
            } 
            return CONTINUE;
        }
        
        private int iter;
        private JIniFile iniFile = null;
        private final String extension;
    }
     
    private class ReplaceFormVarsFileVisitor extends SimpleFileVisitor<Path> {
        
        public ReplaceFormVarsFileVisitor(String ext){
            this.extension = ext.toLowerCase();
            pasFormReplacer.setIgnoreWordWithFPrefix(ignoreWordsWithFPrefix);
        }
        
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
            String currExtension = FileHelper.getExtension(file.getFileName().toString()).toLowerCase();
            if (onlyInPrjFiles)
                if (!isFileInProject(file)) return CONTINUE;
            if (attr.isRegularFile() && currExtension.equals(extension)) {
                pasFormReplacer.replase(file.toString(), "TOracleSession", "ISession");
                pasFormReplacer.replase(file.toString(), "TOracleDataSet", "IDataSet");
                pasFormReplacer.replase(file.toString(), "TOracleQuery", "IQuery");
                progressBarProcess.process(++iter);
                MainForm.writelnMsg("Replaced in " + file.toString());
            } 
            return CONTINUE;
        }
        
        private final String extension;
    }
    
    private class ReplaceDfmVarsVisitor extends SimpleFileVisitor<Path> {

        public ReplaceDfmVarsVisitor(String ext){
            this.extension = ext.toLowerCase();
            replacer = new FileDataReplacer();
            iter = 0;
        }
        
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
            String currExtension = FileHelper.getExtension(file.getFileName().toString()).toLowerCase();
            if (onlyInPrjFiles)
                if (!isFileInProject(file)) return CONTINUE;
            if (attr.isRegularFile() && currExtension.equals(extension)) {
                replacer.setFileReplacerFlag(StringReplacer.WHOLE_WORD | StringReplacer.CASE_INSENSITIVE);
                replacer.replase(file.toString(), "MainDataModule", "UMainDataModule");
                replacer.replase(file.toString(), "DataModule1", "MainDataModule");
                progressBarProcess.process(++iter);
                MainForm.writelnMsg("Replaced in " + file.toString());
            } 
            return CONTINUE;
        }
        
        private int iter;
        private final FileDataReplacer replacer;
        private final String extension;
    }
    
    private class ReplaceModuleVarsVisitor extends SimpleFileVisitor<Path> {

        public ReplaceModuleVarsVisitor(String ext){
            this.extension = ext.toLowerCase();
            replacer = new FileDataReplacer();
            JIniFile iniFile = new JIniFile(iniFileName);
            normalSearch = iniFile.getSection("NormalSearch");
            regexSearch = iniFile.getSection("RegexSearch");
            iter = 0;
        }
        
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
            String currExtension = FileHelper.getExtension(file.getFileName().toString()).toLowerCase();
            if (onlyInPrjFiles)
                if (!isFileInProject(file)) return CONTINUE;
            if (attr.isRegularFile() && currExtension.equals(extension)) {
                replacer.setFileReplacerFlag(StringReplacer.REGEX_FIND | StringReplacer.CASE_INSENSITIVE);
                regexSearch.forEach((x, y) -> replacer.replase(file.toString(), x, y));
                replacer.setFileReplacerFlag(StringReplacer.WHOLE_WORD | StringReplacer.CASE_INSENSITIVE);
                normalSearch.forEach((x, y) -> replacer.replase(file.toString(), x, y));
                progressBarProcess.process(++iter);
                MainForm.writelnMsg("Replaced in " + file.toString());
            } 
            return CONTINUE;
        }
        
        private int iter;
        private final FileDataReplacer replacer;
        private final String extension;
        private Map<String, String> normalSearch, regexSearch;
    }
     
    private class InsertUsesFileVisitor extends SimpleFileVisitor<Path> {
        
        public InsertUsesFileVisitor(String ext){
            this.extension = ext.toLowerCase();
            iter = 0;
        }
        
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
            String currExtension = FileHelper.getExtension(file.getFileName().toString()).toLowerCase();
            if (onlyInPrjFiles)
                if (!isFileInProject(file)) return CONTINUE;
            if (attr.isRegularFile() && currExtension.equals(extension)) {
                PasUsesEditor pasInserter = new PasUsesEditor(file.toString(), iniFileName);
                pasInserter.insertUses();
                pasInserter.removeUses();
                progressBarProcess.process(++iter);
                MainForm.writelnMsg("Inserted into " + file.toString());
            } 
            return CONTINUE;
        }
        
        private int iter;
        private final String extension;
    }

    private class ReplacePropertiesVisitor extends SimpleFileVisitor<Path> {

        public ReplacePropertiesVisitor(String ext){
            this.extension = ext.toLowerCase();
            JIniFile iniFile = new JIniFile(iniFileName);
            editor = new PropertiesEditor(iniFile);
            iter = 0;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
            String currExtension = FileHelper.getExtension(file.getFileName().toString()).toLowerCase();
            if (onlyInPrjFiles)
                if (!isFileInProject(file)) return CONTINUE;
            if (attr.isRegularFile() && currExtension.equals(extension)) {
                MainForm.writelnMsg("Replace in " + file.toString());
                editor.replace(file.toString());
                progressBarProcess.process(++iter);
            }
            return CONTINUE;
        }

        private int iter;
        private final PropertiesEditor editor;
        private final String extension;
    }
    
    private class ChangeButtonsStyle extends SimpleFileVisitor<Path> {

        public ChangeButtonsStyle(String ext){
            this.extension = ext.toLowerCase();
            JIniFile iniFile = new JIniFile(iniFileName);
            changer = new VisualComponentsChanger(iniFile);
            iter = 0;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
            String currExtension = FileHelper.getExtension(file.getFileName().toString()).toLowerCase();
            if (onlyInPrjFiles)
                if (!isFileInProject(file)) return CONTINUE;
            if (attr.isRegularFile() && currExtension.equals(extension)) {
                changer.changeButtonsStyle(file.toString());
                progressBarProcess.process(++iter);
                MainForm.writelnMsg("Changed in " + file.toString());
            }
            return CONTINUE;
        }

        private int iter;
        private final VisualComponentsChanger changer;
        private final String extension;
    }
    
    private class ReplaceBaseClassVisitor extends SimpleFileVisitor<Path> {

        public ReplaceBaseClassVisitor(String ext){
            this.extension = ext.toLowerCase();
            JIniFile iniFile = new JIniFile(iniFileName);
            replacer = new BaseClassReplacer(iniFile);
            iter = 0;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
            String currExtension = FileHelper.getExtension(file.getFileName().toString()).toLowerCase();
            if (onlyInPrjFiles)
                if (!isFileInProject(file)) return CONTINUE;
            if (attr.isRegularFile() && currExtension.equals(extension)) {
                replacer.replace(FileHelper.getFileNameWithoutExt(file.toString()) + ".pas", file.toString());
                progressBarProcess.process(++iter);
                MainForm.writelnMsg("Replaced in " + file.toString());
            }
            return CONTINUE;
        }

        private int iter;
        private final BaseClassReplacer replacer;
        private final String extension;
    }
    
    private class CountFilesVisitor extends SimpleFileVisitor<Path> {
        
        public CountFilesVisitor(String[] exts){
            this.extensions = exts;
            count = 0;
        }
        
        public int getCount(){
            return count;
        }
        
        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
            String currExtension = FileHelper.getExtension(file.getFileName().toString()).toLowerCase();
            if (onlyInPrjFiles)
                if (!isFileInProject(file)) return CONTINUE;
            if (attr.isRegularFile() && Arrays.stream(extensions).anyMatch(s -> s.toLowerCase().equals(currExtension))) 
                ++count;
            return CONTINUE;
        }
        
        private int count;
        private final String[] extensions;
    }
   
    private static int iter;
    private final ProgressBarProcess progressBarProcess;
    private final String projectFile;
    private final String rootFolder;
    private final String iniFileName;
    private final boolean ignoreWordsWithFPrefix;
    private final boolean onlyInPrjFiles;
    private final PasFormReplacer pasFormReplacer = new PasFormReplacer();
}
