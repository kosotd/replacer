package com.kosotd.replacer.UI;

import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyledDocument;
import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.FileHelpers.JIniFile;
import com.kosotd.replacer.Pas.XMLBuilder;
import com.kosotd.replacer.Replacer;
import java.io.FilenameFilter;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class MainForm extends javax.swing.JFrame {

    private static MainForm instance;
    
    public static void writeMsg(String msg){
        StyledDocument doc = instance.tpInfo.getStyledDocument();
        try {
            SimpleAttributeSet keyWord = new SimpleAttributeSet();
            doc.insertString(doc.getLength(), msg, keyWord );
        } catch (BadLocationException ex) {
        }
    }
    
    public static void writelnMsg(String msg){
        writeMsg(msg + "\n");
    }
    
    public MainForm() {
        initComponents();
        listModel = new DefaultListModel();
        listKeyValue.setModel(listModel);
        loadPrefs();
    }
    
    public void setStatus(String msg){
        statusPanelLab1.setText(msg);
    }
       
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        listKeyValuePopup = new javax.swing.JPopupMenu();
        btnAddValue = new javax.swing.JMenuItem();
        btnChangeValue = new javax.swing.JMenuItem();
        btnRemoveValue = new javax.swing.JMenuItem();
        tpInfoPopup = new javax.swing.JPopupMenu();
        btnClear = new javax.swing.JMenuItem();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        tpInfo = new javax.swing.JTextPane();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        panelSettings = new javax.swing.JPanel();
        cbReplaceFormVars = new javax.swing.JCheckBox();
        cbReplaceDfmVars = new javax.swing.JCheckBox();
        cbReplaceModuleVars = new javax.swing.JCheckBox();
        cbInsertUses = new javax.swing.JCheckBox();
        btnWalkFileTree = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        tfRootFolder = new javax.swing.JTextField();
        btnRootFolder = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        tfReplacedNames = new javax.swing.JTextField();
        btnReplacedNames = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        btnStoreToIniFile = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        tfProjectFile = new javax.swing.JTextField();
        btnProjectFile = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        cbOnlyInPrjFiles = new javax.swing.JCheckBox();
        cbIgnoreWithFPrefix = new javax.swing.JCheckBox();
        progressBar = new javax.swing.JProgressBar();
        cbReplaceBaseClass = new javax.swing.JCheckBox();
        cbReplaceProperties = new javax.swing.JCheckBox();
        cbChangeButtonsStyle = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        tfIniFile = new javax.swing.JTextField();
        btnIniFile = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        cbSections = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        listKeyValue = new javax.swing.JList<>();
        btnOpenIniFile = new javax.swing.JButton();
        btnSaveIniAs = new javax.swing.JButton();
        btnAddSection = new javax.swing.JButton();
        btnRemoveSection = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        tfIniFileXmlBuilder = new javax.swing.JTextField();
        btnIniFileXmlBuilder = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        btnBuildXmlFile = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        tfXmlFile = new javax.swing.JTextField();
        btnXmlFile = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        progressBarBuildXml = new javax.swing.JProgressBar();
        jPanel9 = new javax.swing.JPanel();
        tfXmlBuilderRootFolder = new javax.swing.JTextField();
        btnXmlBuilderRootFolder = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        statusPanel = new javax.swing.JPanel();
        statusPanelLab1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        btnExit = new javax.swing.JMenuItem();

        btnAddValue.setText("Add value");
        btnAddValue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddValueActionPerformed(evt);
            }
        });
        listKeyValuePopup.add(btnAddValue);

        btnChangeValue.setText("Change value");
        btnChangeValue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChangeValueActionPerformed(evt);
            }
        });
        listKeyValuePopup.add(btnChangeValue);

        btnRemoveValue.setText("Remove value");
        btnRemoveValue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveValueActionPerformed(evt);
            }
        });
        listKeyValuePopup.add(btnRemoveValue);

        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });
        tpInfoPopup.add(btnClear);

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        tpInfo.setEditable(false);
        tpInfo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tpInfoMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tpInfo);

        jSplitPane1.setBottomComponent(jScrollPane1);

        cbReplaceFormVars.setText("Replace oracle DB items to abstract DB items");

        cbReplaceDfmVars.setText("Replace data module name in .dfm files");

        cbReplaceModuleVars.setText("Replace used names in .pas files");

        cbInsertUses.setText("Insert module names in uses");

        btnWalkFileTree.setText("Start walk file tree");
        btnWalkFileTree.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnWalkFileTreeActionPerformed(evt);
            }
        });

        btnRootFolder.setText("...");
        btnRootFolder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRootFolderActionPerformed(evt);
            }
        });

        jLabel1.setText("Root folder");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(tfRootFolder)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRootFolder, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfRootFolder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRootFolder)))
        );

        btnReplacedNames.setText("...");
        btnReplacedNames.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReplacedNamesActionPerformed(evt);
            }
        });

        jLabel2.setText("Replaced names (ini file)");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(tfReplacedNames)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnReplacedNames, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfReplacedNames, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnReplacedNames)))
        );

        btnStoreToIniFile.setText("Store used types and functions to ini file");
        btnStoreToIniFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStoreToIniFileActionPerformed(evt);
            }
        });

        btnProjectFile.setText("...");
        btnProjectFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProjectFileActionPerformed(evt);
            }
        });

        jLabel5.setText("Project file");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addComponent(tfProjectFile)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnProjectFile, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfProjectFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnProjectFile)))
        );

        cbOnlyInPrjFiles.setText("Only files exists in project");

        cbIgnoreWithFPrefix.setText("Ignore items with F prefix");

        cbReplaceBaseClass.setText("Replace base class");

        cbReplaceProperties.setText("Replace properties");

        cbChangeButtonsStyle.setText("Change buttons style");

        javax.swing.GroupLayout panelSettingsLayout = new javax.swing.GroupLayout(panelSettings);
        panelSettings.setLayout(panelSettingsLayout);
        panelSettingsLayout.setHorizontalGroup(
            panelSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSettingsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelSettingsLayout.createSequentialGroup()
                        .addComponent(btnWalkFileTree)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnStoreToIniFile)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, 339, Short.MAX_VALUE))
                    .addGroup(panelSettingsLayout.createSequentialGroup()
                        .addGroup(panelSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbInsertUses)
                            .addGroup(panelSettingsLayout.createSequentialGroup()
                                .addGroup(panelSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cbReplaceFormVars)
                                    .addGroup(panelSettingsLayout.createSequentialGroup()
                                        .addGap(21, 21, 21)
                                        .addComponent(cbIgnoreWithFPrefix))
                                    .addComponent(cbReplaceDfmVars)
                                    .addComponent(cbReplaceModuleVars))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cbChangeButtonsStyle)
                                    .addComponent(cbReplaceProperties)
                                    .addComponent(cbReplaceBaseClass)
                                    .addComponent(cbOnlyInPrjFiles))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelSettingsLayout.setVerticalGroup(
            panelSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelSettingsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbReplaceFormVars)
                    .addComponent(cbOnlyInPrjFiles))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbIgnoreWithFPrefix)
                    .addComponent(cbReplaceBaseClass))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbReplaceDfmVars)
                    .addComponent(cbReplaceProperties))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbReplaceModuleVars)
                    .addComponent(cbChangeButtonsStyle))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbInsertUses)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelSettingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnWalkFileTree)
                        .addComponent(btnStoreToIniFile))
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Files editor", panelSettings);

        btnIniFile.setText("...");
        btnIniFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniFileActionPerformed(evt);
            }
        });

        jLabel3.setText("Ini file");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(tfIniFile)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnIniFile, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfIniFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnIniFile)))
        );

        cbSections.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbSectionsItemStateChanged(evt);
            }
        });

        jLabel4.setText("Section");

        listKeyValue.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                listKeyValueMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(listKeyValue);

        btnOpenIniFile.setText("Open ini file");
        btnOpenIniFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOpenIniFileActionPerformed(evt);
            }
        });

        btnSaveIniAs.setText("Save as...");
        btnSaveIniAs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveIniAsActionPerformed(evt);
            }
        });

        btnAddSection.setText("Add section");
        btnAddSection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddSectionActionPerformed(evt);
            }
        });

        btnRemoveSection.setText("Remove section");
        btnRemoveSection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveSectionActionPerformed(evt);
            }
        });

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 697, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(btnOpenIniFile)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSave)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSaveIniAs))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbSections, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAddSection)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnRemoveSection)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOpenIniFile)
                    .addComponent(btnSaveIniAs)
                    .addComponent(btnSave))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbSections, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(btnAddSection)
                    .addComponent(btnRemoveSection))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Ini files editor", jPanel3);

        btnIniFileXmlBuilder.setText("...");
        btnIniFileXmlBuilder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniFileXmlBuilderActionPerformed(evt);
            }
        });

        jLabel6.setText("Ini file");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addComponent(tfIniFileXmlBuilder, javax.swing.GroupLayout.DEFAULT_SIZE, 651, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnIniFileXmlBuilder, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfIniFileXmlBuilder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnIniFileXmlBuilder)))
        );

        btnBuildXmlFile.setText("Build XML file");
        btnBuildXmlFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuildXmlFileActionPerformed(evt);
            }
        });

        btnXmlFile.setText("...");
        btnXmlFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXmlFileActionPerformed(evt);
            }
        });

        jLabel7.setText("XML File");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                        .addComponent(tfXmlFile, javax.swing.GroupLayout.DEFAULT_SIZE, 651, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnXmlFile, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfXmlFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnXmlFile)))
        );

        btnXmlBuilderRootFolder.setText("...");
        btnXmlBuilderRootFolder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXmlBuilderRootFolderActionPerformed(evt);
            }
        });

        jLabel8.setText("Root folder");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addComponent(tfXmlBuilderRootFolder, javax.swing.GroupLayout.DEFAULT_SIZE, 651, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnXmlBuilderRootFolder, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfXmlBuilderRootFolder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnXmlBuilderRootFolder)))
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addComponent(progressBarBuildXml, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuildXmlFile))
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 122, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBuildXmlFile, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(progressBarBuildXml, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("GuidesXMLBuilder", jPanel6);

        jSplitPane1.setLeftComponent(jTabbedPane1);

        statusPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelLab1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelLab1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 17, Short.MAX_VALUE)
        );

        jMenu1.setText("File");

        btnExit.setText("Exit");
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });
        jMenu1.add(btnExit);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSplitPane1)
                    .addComponent(statusPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 488, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnReplacedNamesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReplacedNamesActionPerformed
        selectIniFile(tfReplacedNames, LAST_REPLACED_NAMES_FILE_DIR);
    }//GEN-LAST:event_btnReplacedNamesActionPerformed

    private void btnRootFolderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRootFolderActionPerformed
        Preferences prefs = Preferences.userRoot().node(getClass().getName());
        JFileChooser dialog = new JFileChooser(prefs.get(LAST_ROOT_FOLDER_DIR, new File(".").getAbsolutePath())); 
        dialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        dialog.setAcceptAllFileFilterUsed(false);
        if (dialog.showDialog(null, "Select root directory") == JFileChooser.APPROVE_OPTION) {
            File file = dialog.getSelectedFile();
            tfRootFolder.setText(file.toString());
            prefs.put(LAST_ROOT_FOLDER_DIR, dialog.getSelectedFile().getParent());
        }
    }//GEN-LAST:event_btnRootFolderActionPerformed

    private void selectIniFile(JTextField textField, String prefVal){
        Preferences prefs = Preferences.userRoot().node(getClass().getName());
        JFileChooser dialog = new JFileChooser(prefs.get(prefVal, new File(".").getAbsolutePath()));    
        dialog.setFileFilter(iniFilesFilter);
        dialog.setAcceptAllFileFilterUsed(false);
        if (dialog.showDialog(null, "Select ini file") == JFileChooser.APPROVE_OPTION) {
            File file = dialog.getSelectedFile();
            textField.setText(file.toString());
            prefs.put(prefVal, dialog.getSelectedFile().getParent());
        }
    }
    
    private void btnIniFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniFileActionPerformed
        selectIniFile(tfIniFile, LAST_INI_FILE_DIR);
    }//GEN-LAST:event_btnIniFileActionPerformed

    private void btnWalkFileTreeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnWalkFileTreeActionPerformed
        if (!validateFields(false)) return;
        new Thread(() -> {
            Replacer replacer = new Replacer(tfReplacedNames.getText(), tfRootFolder.getText(), 
                tfProjectFile.getText(), cbIgnoreWithFPrefix.isSelected(), 
                cbOnlyInPrjFiles.isSelected(), new ProgressBarProcessImpl(progressBar));
            if (cbReplaceFormVars.isSelected()){
                setStatus("Replace oracle DB items to abstract DB items");
                replacer.ReplaceFormVars();
            }
            if (cbReplaceDfmVars.isSelected()){
                setStatus("Replace data module name in .dfm files");
                replacer.ReplaceDfmVars();
            }
            if (cbReplaceModuleVars.isSelected()){
                setStatus("Replace used names in .pas files");
                replacer.ReplaceModuleVars();
            }
            if (cbReplaceProperties.isSelected()){
                setStatus("Replace properties in .pas files");
                replacer.ReplaceProperties();
            }
            if (cbInsertUses.isSelected()){
                setStatus("Insert module names in uses");
                replacer.InsertUses();
            }
            if (cbReplaceBaseClass.isSelected()){
                setStatus("Replace base class");
                replacer.ReplaceBaseClass();
            }
            if (cbChangeButtonsStyle.isSelected()){
                setStatus("Change buttons style");
                replacer.ChangeButtonsStyle();
            }
            setStatus("");
            JOptionPane.showMessageDialog(null, "Sucsess");
        }).start();
        storePrefs();
    }//GEN-LAST:event_btnWalkFileTreeActionPerformed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        if (checkIniFile() != JOptionPane.CANCEL_OPTION) 
            dispose();
    }//GEN-LAST:event_btnExitActionPerformed

    private void btnStoreToIniFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStoreToIniFileActionPerformed
        if (!validateFields(true)) return;
        new Thread(() -> {
            Replacer replacer = new Replacer(tfReplacedNames.getText(), tfRootFolder.getText(), 
                tfProjectFile.getText(), cbIgnoreWithFPrefix.isSelected(), 
                cbOnlyInPrjFiles.isSelected(), new ProgressBarProcessImpl(progressBar));
            setStatus("Store used types and functions to ini file");
            replacer.StoreTypesAndFuncsToIniFile();
            setStatus("");
            JOptionPane.showMessageDialog(null, "Sucsess");
        }).start();  
        storePrefs();
    }//GEN-LAST:event_btnStoreToIniFileActionPerformed

    private int checkIniFile(){
        int result = JOptionPane.OK_OPTION;
        if (iniFile != null)
            if (iniFile.isModified()){
                result = JOptionPane.showConfirmDialog(
                    this,
                    "Ini file was modified. Do you want save it?",
                    "Save modified",
                    JOptionPane.YES_NO_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION)
                    iniFile.updateFile();
            }
        return result;
    }
    
    private void btnOpenIniFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOpenIniFileActionPerformed
        File file = new File(tfIniFile.getText());
        if (!file.exists()) return;
        if (!FileHelper.getExtension(file).toLowerCase().equals("ini")) return;
        if (checkIniFile() == JOptionPane.CANCEL_OPTION) return;
        iniFile = new JIniFile(tfIniFile.getText());
        List<String> sections = iniFile.readSections();
        cbSections.removeAllItems();
        sections.forEach(s -> cbSections.addItem(s));
        storePrefs();
    }//GEN-LAST:event_btnOpenIniFileActionPerformed

    private void cbSectionsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbSectionsItemStateChanged
        Map<String, String> section = iniFile.getSection(evt.getItem().toString());
        if (cbSections.getSelectedItem() == null) section.clear();
        listModel.clear();
        listModel = new DefaultListModel();
        section.forEach((k, v) -> {
            listModel.addElement(new ListKeyValueElement(k, v));
        });
        listKeyValue.setModel(listModel);
    }//GEN-LAST:event_cbSectionsItemStateChanged

    private void btnProjectFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProjectFileActionPerformed
        Preferences prefs = Preferences.userRoot().node(getClass().getName());
        JFileChooser dialog = new JFileChooser(prefs.get(LAST_PROJECT_FILE_DIR, new File(".").getAbsolutePath()));    
        dialog.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return FileHelper.getExtension(f).toLowerCase().equals("dpr") || f.isDirectory();
            }

            @Override
            public String getDescription() {
                return "Project files";
            }
        });
        dialog.setAcceptAllFileFilterUsed(false);
        if (dialog.showDialog(null, "Select project file") == JFileChooser.APPROVE_OPTION) {
            File file = dialog.getSelectedFile();
            tfProjectFile.setText(file.toString());
            prefs.put(LAST_PROJECT_FILE_DIR, dialog.getSelectedFile().getParent());
        }
    }//GEN-LAST:event_btnProjectFileActionPerformed

    private void listKeyValueMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listKeyValueMouseReleased
        if (evt.getButton() == MouseEvent.BUTTON3){
            checkMenuItemsAvailability();
            listKeyValuePopup.show(listKeyValue, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_listKeyValueMouseReleased

    private void btnAddValueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddValueActionPerformed
        String key, value;
        do {
            key = JOptionPane.showInputDialog(this, "Enter the key");
            if (key == null) return;
            key = key.trim();
        } while(!isValidKey(key));
        do {
            value = JOptionPane.showInputDialog(this, "Enter the value");
            if (value == null) return;
            value = value.trim();
        } while(!isValidKey(value));
        if (iniFile.valueExist(cbSections.getSelectedItem().toString(), key)){
            JOptionPane.showMessageDialog(null, "Key with name " + key + " already exist");
            return;
        }
        listModel.addElement(new ListKeyValueElement(key, value));
        iniFile.writeString(cbSections.getSelectedItem().toString(), key, value);
    }//GEN-LAST:event_btnAddValueActionPerformed

    private void btnChangeValueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChangeValueActionPerformed
        String value;
        do {
            value = JOptionPane.showInputDialog(this, "Enter the value");
            if (value == null) return;
            value = value.trim();
        } while(!isValidKey(value));
        ListKeyValueElement elem = (ListKeyValueElement)listModel.get(listKeyValue.getSelectedIndex());
        elem.setValue(value);
        iniFile.writeString(cbSections.getSelectedItem().toString(), elem.getKey(), value);
    }//GEN-LAST:event_btnChangeValueActionPerformed

    private void btnRemoveValueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveValueActionPerformed
        int[] inds = listKeyValue.getSelectedIndices();
        if (inds == null) return;
        List<ListKeyValueElement> removeElems = new ArrayList<>();
        for (int i : inds){
            ListKeyValueElement elem = (ListKeyValueElement)listModel.get(i);
            removeElems.add(elem);
        }
        for (int i = 0; i < removeElems.size(); ++i){
            iniFile.deleteKey(cbSections.getSelectedItem().toString(), removeElems.get(i).getKey());
            listModel.removeElement(removeElems.get(i));
        }
    }//GEN-LAST:event_btnRemoveValueActionPerformed

    private void btnSaveIniAsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveIniAsActionPerformed
        if (iniFile == null) return;
        Preferences prefs = Preferences.userRoot().node(getClass().getName());
        JFileChooser dialog = new JFileChooser(prefs.get(LAST_SAVE_AS_DIR, new File(".").getAbsolutePath()));    
        dialog.setFileFilter(iniFilesFilter);
        dialog.setAcceptAllFileFilterUsed(false);
        if (dialog.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            File file = dialog.getSelectedFile();
            if (!file.exists()){
                try {
                    file.createNewFile();
                } catch (IOException ex) { }
            }
            iniFile.updateToFile(file);
            prefs.put(LAST_SAVE_AS_DIR, dialog.getSelectedFile().getParent());
            iniFile = new JIniFile(file.getAbsolutePath());
            tfIniFile.setText(file.getAbsolutePath());
            storePrefs();
        }
    }//GEN-LAST:event_btnSaveIniAsActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (checkIniFile() != JOptionPane.CANCEL_OPTION) 
            dispose();
    }//GEN-LAST:event_formWindowClosing

    private void btnAddSectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddSectionActionPerformed
        if (iniFile == null) return;
        String section;
        do {
            section = JOptionPane.showInputDialog(this, "Enter the section");
            if (section == null) return;
            section = section.trim();
        } while(!isValidKey(section));
        if (iniFile.sectionExist(section)){
            JOptionPane.showMessageDialog(null, "Section with name " + section + " already exist");
            return;
        }
        cbSections.addItem(section);
    }//GEN-LAST:event_btnAddSectionActionPerformed
    
    private void btnRemoveSectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveSectionActionPerformed
        if (iniFile == null) return;
        if (cbSections.getSelectedIndex() < 0) return;
        String section = cbSections.getSelectedItem().toString();
        cbSections.removeItemAt(cbSections.getSelectedIndex());
        iniFile.eraseSection(section);
    }//GEN-LAST:event_btnRemoveSectionActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        if (iniFile == null) return;
        iniFile.updateFile();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        tpInfo.setText("");
    }//GEN-LAST:event_btnClearActionPerformed

    private void tpInfoMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tpInfoMouseReleased
        if (evt.getButton() == MouseEvent.BUTTON3){
            tpInfoPopup.show(tpInfo, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_tpInfoMouseReleased

    private void btnIniFileXmlBuilderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniFileXmlBuilderActionPerformed
        selectIniFile(tfIniFileXmlBuilder, LAST_XML_BUILDER_INI_FILE_DIR);
    }//GEN-LAST:event_btnIniFileXmlBuilderActionPerformed

    private void btnXmlFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXmlFileActionPerformed
        Preferences prefs = Preferences.userRoot().node(getClass().getName());
        JFileChooser dialog = new JFileChooser(prefs.get(LAST_XML_FILE_DIR, new File(".").getAbsolutePath()));    
        dialog.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return FileHelper.getExtension(f).toLowerCase().equals("xml") || f.isDirectory();
            }

            @Override
            public String getDescription() {
                return "XML files";
            }
        });
        dialog.setAcceptAllFileFilterUsed(false);
        dialog.setFileSelectionMode(WIDTH);
        dialog.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        if (dialog.showDialog(null, "Select XML file") == JFileChooser.APPROVE_OPTION) {
            File file = dialog.getSelectedFile();
            tfXmlFile.setText(file.toString());
            prefs.put(LAST_XML_FILE_DIR, dialog.getSelectedFile().getParent());
        }
    }//GEN-LAST:event_btnXmlFileActionPerformed

    private void btnBuildXmlFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuildXmlFileActionPerformed
        if (!validateXmlBuilderFields()) return;
        JIniFile iniFile = new JIniFile(tfIniFileXmlBuilder.getText());
        List<String> withoutSelectData = iniFile.getSection("GuideFormNames")
                .entrySet().stream().map(e -> e.getKey()).collect(Collectors.toList());
        ProgressBarProcess process = new ProgressBarProcessImpl(progressBarBuildXml);
        process.init(withoutSelectData.size());  
        AtomicInteger counter = new AtomicInteger(0);
        List<String> dataModules = iniFile.getSection("DataModules")
                .entrySet().stream().map(e -> e.getValue()).collect(Collectors.toList());
        XMLBuilder buider = new XMLBuilder(tfIniFileXmlBuilder.getText(), tfXmlFile.getText(),
                getDataModuleFiles(tfXmlBuilderRootFolder.getText(), dataModules));
        setStatus("Storing guides to xml");
        List<File> files = getGuideFiles(withoutSelectData, tfXmlBuilderRootFolder.getText());
        files.stream().forEach(s -> {   
            writelnMsg("Store form " + s.getName());
            buider.accumulate(FileHelper.getFileNameWithoutExt(s.getAbsoluteFile()));
            process.process(counter.incrementAndGet());
        });
        buider.storeToXml();        
        setStatus("");
        storePrefs();
        JOptionPane.showMessageDialog(null, "Sucsess");
    }//GEN-LAST:event_btnBuildXmlFileActionPerformed

    private Map<String, File> getDataModuleFiles(String directory, List<String> dataModules){
        File dir = new File(directory);
        Map<String, File> res = new HashMap<>();
        Arrays.stream(dir.listFiles()).forEach(f -> {
            if (f.isDirectory()){
                res.putAll(getDataModuleFiles(f.getAbsolutePath(), dataModules));
            } else if (f.isFile()){
                String s = FileHelper.getFileNameWithoutExt(f.getName()).trim().toLowerCase();
                if (dataModules.stream().anyMatch(x -> x.trim().toLowerCase().equals(s)) && f.getName().endsWith(".dfm"))
                    res.put(s, f);
            }
        });
        return res;
    }

    private void btnXmlBuilderRootFolderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXmlBuilderRootFolderActionPerformed
        Preferences prefs = Preferences.userRoot().node(getClass().getName());
        JFileChooser dialog = new JFileChooser(prefs.get(LAST_XML_BUILDER_ROOT_FOLDER_DIR, new File(".").getAbsolutePath())); 
        dialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        dialog.setAcceptAllFileFilterUsed(false);
        if (dialog.showDialog(null, "Select root directory") == JFileChooser.APPROVE_OPTION) {
            File file = dialog.getSelectedFile();
            tfXmlBuilderRootFolder.setText(file.toString());
            prefs.put(LAST_XML_BUILDER_ROOT_FOLDER_DIR, dialog.getSelectedFile().getParent());
        }
    }//GEN-LAST:event_btnXmlBuilderRootFolderActionPerformed
    
    private List<File> getGuideFiles(List<String> guides, String directory){
        File dir = new File(directory);
        List<File> res = new ArrayList<>();
        Arrays.stream(dir.listFiles()).forEach(f -> {
            if (f.isDirectory()){
                res.addAll(getGuideFiles(guides, f.getAbsolutePath()));
            } else if (f.isFile()){                
                String s = FileHelper.getFileNameWithoutExt(f.getName()).trim().toLowerCase();
                if (guides.stream().anyMatch(x -> x.trim().toLowerCase().equals(s)) && f.getName().endsWith(".dfm"))
                    res.add(f);
            }
        });
        return res;
    }
    
    private boolean validateXmlBuilderFields(){
        File iniFileCheck = new File(tfIniFileXmlBuilder.getText());
        if ((!FileHelper.getExtension(iniFileCheck).toLowerCase().equals("ini")) ||
            (!iniFileCheck.exists())) return false;
        File xmlFile = new File(tfXmlFile.getText());
        if (!FileHelper.getExtension(xmlFile).toLowerCase().equals("xml")) return false;
        File rootFolder = new File(tfXmlBuilderRootFolder.getText());
        if ((!rootFolder.exists()) || (!rootFolder.isDirectory())) return false;
        return true;
    }
    
    private boolean isValidKey(String key){
        Pattern keyPattern = Pattern.compile("[^=\\s]+", Pattern.CASE_INSENSITIVE);
        Matcher keyMatcher = keyPattern.matcher(key);
        boolean res = false;
        if (keyMatcher.matches())
            if (!(keyMatcher.group().startsWith("[") && keyMatcher.group().endsWith("]")))
                res = true;
        
        return res;
    }
    
    private void checkMenuItemsAvailability(){
        btnAddValue.setEnabled(cbSections.getSelectedIndex() > -1);
        int selectedCount = 0;
        int[] inds = listKeyValue.getSelectedIndices();
        if (inds != null)
            selectedCount = inds.length;
        btnChangeValue.setEnabled(selectedCount == 1);
        btnRemoveValue.setEnabled(selectedCount > 0);
    }
    
    private boolean validateFields(boolean createIniIfNotExist){
        File projectFile = new File(tfProjectFile.getText());
        if (cbOnlyInPrjFiles.isSelected())
            if ((!projectFile.exists()) || 
                (!FileHelper.getExtension(projectFile).toLowerCase().equals("dpr"))) return false;
        File rootFolder = new File(tfRootFolder.getText());
        if ((!rootFolder.exists()) || (!rootFolder.isDirectory())) return false;
        File replacedNames = new File(tfReplacedNames.getText());
        if (!FileHelper.getExtension(replacedNames).toLowerCase().equals("ini")) return false;
        if (!replacedNames.exists()) {
            if (createIniIfNotExist) {
                try {
                    return replacedNames.createNewFile();
                } catch (IOException ex) { }
            } else 
                return false;
        }
        return true;
    }
    
    private void storePrefs(){
        Preferences prefs = Preferences.userRoot().node(getClass().getName());
        prefs.put(LAST_INI_FILE, tfIniFile.getText());
        prefs.put(LAST_REPLACED_NAMES_FILE, tfReplacedNames.getText());
        prefs.put(LAST_ROOT_FOLDER, tfRootFolder.getText());
        prefs.put(LAST_PROJECT_FILE, tfProjectFile.getText());
        prefs.put(LAST_XML_BUILDER_INI_FILE, tfIniFileXmlBuilder.getText());
        prefs.put(LAST_XML_FILE, tfXmlFile.getText());
        prefs.put(LAST_XML_BUILDER_ROOT_FOLDER, tfXmlBuilderRootFolder.getText());
        prefs.putBoolean(REPLACE_FORM_VARS, cbReplaceFormVars.isSelected());
        prefs.putBoolean(INSERT_USES, cbInsertUses.isSelected());
        prefs.putBoolean(REPLACE_DFM_VARS, cbReplaceDfmVars.isSelected());
        prefs.putBoolean(REPLACE_MODULE_VARS, cbReplaceModuleVars.isSelected());
        prefs.putBoolean(ONLY_IN_PRJ_FILES, cbOnlyInPrjFiles.isSelected());
        prefs.putBoolean(IGNORE_WORDS_WITH_F_PREFIX, cbIgnoreWithFPrefix.isSelected());
        prefs.putBoolean(REPLACE_BASE_CLASS, cbReplaceBaseClass.isSelected());
        prefs.putBoolean(REPLACE_PROPERTIES, cbReplaceProperties.isSelected());
        prefs.putBoolean(CHANGE_BUTTONS_STYLE, cbChangeButtonsStyle.isSelected());
    }
    
    private void loadPrefs(){
        Preferences prefs = Preferences.userRoot().node(getClass().getName());
        tfIniFile.setText(prefs.get(LAST_INI_FILE, ""));
        tfReplacedNames.setText(prefs.get(LAST_REPLACED_NAMES_FILE, ""));
        tfRootFolder.setText(prefs.get(LAST_ROOT_FOLDER, ""));
        tfProjectFile.setText(prefs.get(LAST_PROJECT_FILE, ""));
        tfIniFileXmlBuilder.setText(prefs.get(LAST_XML_BUILDER_INI_FILE, ""));     
        tfXmlFile.setText(prefs.get(LAST_XML_FILE, "")); 
        tfXmlBuilderRootFolder.setText(prefs.get(LAST_XML_BUILDER_ROOT_FOLDER, ""));         
        cbReplaceFormVars.setSelected(prefs.getBoolean(REPLACE_FORM_VARS, false));
        cbInsertUses.setSelected(prefs.getBoolean(INSERT_USES, false));
        cbReplaceDfmVars.setSelected(prefs.getBoolean(REPLACE_DFM_VARS, false));
        cbReplaceModuleVars.setSelected(prefs.getBoolean(REPLACE_MODULE_VARS, false));
        cbOnlyInPrjFiles.setSelected(prefs.getBoolean(ONLY_IN_PRJ_FILES, false));
        cbIgnoreWithFPrefix.setSelected(prefs.getBoolean(IGNORE_WORDS_WITH_F_PREFIX, false));
        cbReplaceBaseClass.setSelected(prefs.getBoolean(REPLACE_BASE_CLASS, false));
        cbReplaceProperties.setSelected(prefs.getBoolean(REPLACE_PROPERTIES, false));
        cbChangeButtonsStyle.setSelected(prefs.getBoolean(CHANGE_BUTTONS_STYLE, false));
    }
    
    public static void main(String args[]) {
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                instance = new MainForm();
                instance.setLocationRelativeTo(null);
                instance.setVisible(true);
            }
        });
    }
    
    private JIniFile iniFile;
    private DefaultListModel listModel;
    private final String LAST_SAVE_AS_DIR = "LAST_SAVE_AS_DIR";
    private final String LAST_PROJECT_FILE = "LAST_PROJECT_FILE";
    private final String LAST_PROJECT_FILE_DIR = "LAST_PROJECT_FILE_DIR";
    private final String LAST_REPLACED_NAMES_FILE = "LAST_REPLACED_NAMES_FILE";
    private final String LAST_ROOT_FOLDER = "LAST_ROOT_FOLDER";
    private final String LAST_INI_FILE = "LAST_USED_FOLDER";
    private final String LAST_XML_BUILDER_INI_FILE = "LAST_XML_BUILDER_INI_FILE";
    private final String LAST_REPLACED_NAMES_FILE_DIR = "LAST_REPLACED_NAMES_FILE_DIR";
    private final String LAST_ROOT_FOLDER_DIR = "LAST_ROOT_FOLDER_DIR";
    private final String LAST_INI_FILE_DIR = "LAST_INI_FILE_DIR";
    private final String REPLACE_FORM_VARS = "REPLACE_FORM_VARS";
    private final String INSERT_USES = "INSERT_USES";
    private final String CHANGE_BUTTONS_STYLE = "CHANGE_BUTTONS_STYLE";
    private final String REPLACE_DFM_VARS = "REPLACE_DFM_VARS";
    private final String REPLACE_BASE_CLASS = "REPLACE_BASE_CLASS";
    private final String REPLACE_PROPERTIES = "REPLACE_PROPERTIES";
    private final String REPLACE_MODULE_VARS = "REPLACE_MODULE_VARS";
    private final String ONLY_IN_PRJ_FILES = "ONLY_IN_PRJ_FILES";
    private final String IGNORE_WORDS_WITH_F_PREFIX = "IGNORE_WORDS_WITH_F_PREFIX";
    private final String LAST_XML_BUILDER_INI_FILE_DIR = "LAST_XML_BUILDER_INI_FILE_DIR";
    private final String LAST_XML_FILE_DIR = "LAST_XML_FILE_DIR";
    private final String LAST_XML_FILE = "LAST_XML_FILE";
    private final String LAST_XML_BUILDER_ROOT_FOLDER_DIR = "LAST_XML_BUILDER_ROOT_FOLDER_DIR";
    private final String LAST_XML_BUILDER_ROOT_FOLDER = "LAST_XML_BUILDER_ROOT_FOLDER";
    
    private final FileFilter iniFilesFilter = new FileFilter() {
        @Override
        public boolean accept(File f) {
            return FileHelper.getExtension(f).toLowerCase().equals("ini") || f.isDirectory();
        }

        @Override
        public String getDescription() {
            return "Ini files";
        }
    };
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddSection;
    private javax.swing.JMenuItem btnAddValue;
    private javax.swing.JButton btnBuildXmlFile;
    private javax.swing.JMenuItem btnChangeValue;
    private javax.swing.JMenuItem btnClear;
    private javax.swing.JMenuItem btnExit;
    private javax.swing.JButton btnIniFile;
    private javax.swing.JButton btnIniFileXmlBuilder;
    private javax.swing.JButton btnOpenIniFile;
    private javax.swing.JButton btnProjectFile;
    private javax.swing.JButton btnRemoveSection;
    private javax.swing.JMenuItem btnRemoveValue;
    private javax.swing.JButton btnReplacedNames;
    private javax.swing.JButton btnRootFolder;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSaveIniAs;
    private javax.swing.JButton btnStoreToIniFile;
    private javax.swing.JButton btnWalkFileTree;
    private javax.swing.JButton btnXmlBuilderRootFolder;
    private javax.swing.JButton btnXmlFile;
    private javax.swing.JCheckBox cbChangeButtonsStyle;
    private javax.swing.JCheckBox cbIgnoreWithFPrefix;
    private javax.swing.JCheckBox cbInsertUses;
    private javax.swing.JCheckBox cbOnlyInPrjFiles;
    private javax.swing.JCheckBox cbReplaceBaseClass;
    private javax.swing.JCheckBox cbReplaceDfmVars;
    private javax.swing.JCheckBox cbReplaceFormVars;
    private javax.swing.JCheckBox cbReplaceModuleVars;
    private javax.swing.JCheckBox cbReplaceProperties;
    private javax.swing.JComboBox<String> cbSections;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JList<String> listKeyValue;
    private javax.swing.JPopupMenu listKeyValuePopup;
    private javax.swing.JPanel panelSettings;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JProgressBar progressBarBuildXml;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JLabel statusPanelLab1;
    private javax.swing.JTextField tfIniFile;
    private javax.swing.JTextField tfIniFileXmlBuilder;
    private javax.swing.JTextField tfProjectFile;
    private javax.swing.JTextField tfReplacedNames;
    private javax.swing.JTextField tfRootFolder;
    private javax.swing.JTextField tfXmlBuilderRootFolder;
    private javax.swing.JTextField tfXmlFile;
    private javax.swing.JTextPane tpInfo;
    private javax.swing.JPopupMenu tpInfoPopup;
    // End of variables declaration//GEN-END:variables
}
