package com.kosotd.replacer.UI;

public class ListKeyValueElement {
    public ListKeyValueElement(String key, String value){
        this.key = key;
        this.value = value;
    }
    
    public void setValue(String value){
        this.value = value;
    }
    
    public String getKey(){
        return key;
    }
    
    public String getValue(){
        return value;
    }
    
    @Override
    public String toString(){
        return key + " = " + value;
    }
    
    private String key;
    private String value;
}
