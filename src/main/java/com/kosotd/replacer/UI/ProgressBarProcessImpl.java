package com.kosotd.replacer.UI;

import javax.swing.JProgressBar;

public class ProgressBarProcessImpl implements ProgressBarProcess {

    public ProgressBarProcessImpl(JProgressBar progressBar ){
        this.progressBar = progressBar;
    }
    
    @Override
    public void init(int maxIterCount) {
        progressBar.setMinimum(0);
        progressBar.setMaximum(maxIterCount);
        progressBar.setValue(0);
    }

    @Override
    public void process(int currIter) {
        progressBar.setValue(currIter);
    }
    
    private JProgressBar progressBar;
    
}
