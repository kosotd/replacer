package com.kosotd.replacer.UI;

public interface ProgressBarProcess {
    void init(int maxIterCount);
    void process(int currIter);
}
