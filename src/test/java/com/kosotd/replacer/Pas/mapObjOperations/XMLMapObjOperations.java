/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas.mapObjOperations;

import com.kosotd.replacer.FileHelpers.FileHelper;
import com.kosotd.replacer.FileHelpers.JIniFile;
import com.kosotd.replacer.Pas.PasHelpers.PasHelper;
import com.kosotd.replacer.StringHelpers.StringReplacer;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javafx.util.Pair;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class XMLMapObjOperations {
    private LayerInfoItems guides = new LayerInfoItems();
    private String xmlFileName;
    private JIniFile iniFile;
    private String layerInfoItemsFile;
    private LayerInfoItems layerInfoItems;
    
    public static void main(String[] args){
        XMLMapObjOperations xmlMapObjOperations = new XMLMapObjOperations("Settings.ini", "LayerInfoItems.txt",
            "LayerInfoItems.xml");
        xmlMapObjOperations.process();
    }
    
    public XMLMapObjOperations(String iniFileName, String layerInfoItemsFile, String xmlFileName){
        layerInfoItems = new LayerInfoItems();
        iniFile = new JIniFile(iniFileName);
        this.xmlFileName = xmlFileName;
        this.layerInfoItemsFile = layerInfoItemsFile;
    }    
    
    public void process(){
        Map<String, String> layerKeys = iniFile.getSection("LayerKeys");
        String layerInfoItemsFileData = FileHelper.getFileData(layerInfoItemsFile);
        Map<String, String> processingMethods = iniFile.getSection("ProcessingMethods");
        
        String strPattern = 
       "\\bCreate\\s*\\(\\s*([a-zа-я 0-9_'.:;)(]+)\\s*,\\s*([a-zа-я 0-9_'.:;)(]+)\\s*,\\s*([a-zа-я 0-9_'.:;)(]+)"
                + "\\s*,\\s*([a-zа-я 0-9_'.:;)(]+)\\s*,\\s*([a-zа-я 0-9_'.:;)(]+)\\s*,\\s*([a-zа-я 0-9_'.:;)(]+)"
                + "\\s*,\\s*([a-z0-9_'.:;)(]+)\\s*(,\\s*([a-zа-я 0-9_'.:;)(]+)\\s*,\\s*([a-zа-я 0-9_'.:;]+)\\s*)?\\)";
        
        Pattern pattern = Pattern.compile(strPattern, Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.UNICODE_CHARACTER_CLASS);
        Matcher matcher = pattern.matcher(layerInfoItemsFileData);
        String regClss = "RegisterClasses([";
        while (matcher.find()){            
            LayerInfoItem layerInfoItem = new LayerInfoItem();
            String lk = matcher.group(1);
            layerInfoItem.layerKey = layerKeys.entrySet().stream().filter(s-> { 
                return s.getKey().toLowerCase().equals(lk.toLowerCase()); 
            }).findFirst().get().getValue();
            layerInfoItem.layerCaption = matcher.group(2).replaceAll("'", "");
            layerInfoItem.layerName = matcher.group(3).replaceAll("'", "");
            layerInfoItem.objectName = matcher.group(4).replaceAll("'", ""); 
            layerInfoItem.dblLayerScale = matcher.group(5).replaceAll("'", "");
            layerInfoItem.bZoomSelected = matcher.group(6).replaceAll("'", "");            
            String name = FileHelper.getExtension(matcher.group(7)).toLowerCase();
            String gr9 = matcher.group(9) == null ? "" : matcher.group(9);
            String gr10 = matcher.group(10) == null ? "" : matcher.group(10);
            layerInfoItem.schemaName = gr9.replaceAll("'", "");;
            layerInfoItem.formClassName = gr10.replaceAll("'", "");   
            Optional<Map.Entry<String, String>> opt = processingMethods.entrySet().stream().filter(s -> s.getKey().toLowerCase().equals(name)).findFirst();
            layerInfoItem.processingMethod = opt.isPresent() ? opt.get().getValue() : "";
            layerInfoItem.validate();
            layerInfoItems.layerInfoItems.add(layerInfoItem);
            if ("".equals(layerInfoItem.layerKey) || (layerInfoItem.layerKey == null)) 
                throw new AssertionError();
            if (!layerInfoItem.formClassName.equals("")){
                if (!regClss.equals("RegisterClasses(["))
                    regClss += ", ";
                regClss += layerInfoItem.formClassName;
            }
        }
        regClss += "]);";
        System.out.println(regClss);
        storeToXml();
    }
    
    public void storeToIni(){
        Map<String, String> layerKeys = iniFile.getSection("LayerKeys");
        String layerInfoItemsFileData = FileHelper.getFileData(layerInfoItemsFile);
        Pattern pattern = Pattern.compile("\\bprocedure\\s+[a-z0-9_]+\\s*\\.\\s*([a-z0-9_]+)\\b.*?\\bend\\b", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher matcher = pattern.matcher(layerInfoItemsFileData);
        while (matcher.find()){
            String name = matcher.group(1);
            iniFile.writeString("ProcessingMethods", name, "method");
        }
        iniFile.updateFile();
    }
    
    public String getFormClass(String data){
        Pattern pattern = Pattern.compile("\\bShowMapObjectForm\\s*\\(\\s*([a-z0-9_]+)\\s*,", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(data);
        String formClass = "";
        if (matcher.find()){
            formClass = matcher.group(1);
        }
        return formClass;
    }
    
    public String getLayerKey(String data){
        Pattern pattern = Pattern.compile("\\bCheckSelectedLayerName\\s*\\(\\s*([a-z0-9_]+)\\s*\\)", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(data);
        String layerKey = "";
        if (matcher.find()){
            layerKey = matcher.group(1);
        }
        return layerKey;
    }
    
    public void storeToXml(){
        try {
            File file = new File(xmlFileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(LayerInfoItems.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(layerInfoItems, file);
        } catch (JAXBException e) {
        }
    }
    
}
