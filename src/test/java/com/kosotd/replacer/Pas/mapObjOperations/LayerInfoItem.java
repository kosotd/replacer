/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas.mapObjOperations;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User
 */
@XmlRootElement
public class LayerInfoItem {
    public String layerKey;
    public String layerCaption; 
    public String layerName;
    public String objectName;
    public String dblLayerScale;
    public String bZoomSelected;
    public String schemaName;
    public String formClassName;
    public String processingMethod;
    
    public void validate(){
        if ("".equals(layerKey.trim())) 
            throw new AssertionError();
        if ("".equals(layerCaption.trim())) 
            throw new AssertionError();
        if ("".equals(layerName.trim())) 
            throw new AssertionError();
        if ("".equals(objectName.trim())) 
            throw new AssertionError();
        if ("".equals(dblLayerScale.trim())) 
            throw new AssertionError();
        if ("".equals(bZoomSelected.trim())) 
            throw new AssertionError();
        if (processingMethod == null)
            throw new AssertionError();
        if (schemaName == null)
            throw new AssertionError();
        if (formClassName == null)
            throw new AssertionError();
    }
}
