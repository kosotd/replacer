/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas.mapObjOperations;

import com.kosotd.replacer.Pas.Guide;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User
 */
@XmlRootElement(name = "LayerInfoItems")
public class LayerInfoItems {
    @XmlElement(name = "LayerInfoItem")
    public List<LayerInfoItem> layerInfoItems = new ArrayList<>();
}
