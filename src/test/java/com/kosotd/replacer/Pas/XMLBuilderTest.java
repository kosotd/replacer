/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kosotd.replacer.Pas;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author User
 */
public class XMLBuilderTest {
    
    public XMLBuilderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCaption method, of class XMLBuilder.
     */
    @Test
    public void testGetCaption() {
        System.out.println("getCaption");
        String field = 
                "object GuideGridViewSTATE_GP_NAME: TcxGridDBColumn\n" +
                "        Caption = \n" +
                "          #1057#1090#1072#1090#1091#1089#1099' '#1086#1073#1098#1077#1082#1090#1086#1074' '#1082#1072#1087#1080#1090#1072#1083#1100#1085#1086#1075#1086' '#1089#1090#1088#1086#1080#1090#1077#1083#1100#1089#1090#1074#1072' ('#1087#1086' '#1075#1077#1085#1077#1088#1072#1083#1100#1085#1086#1084#1091' '#1087#1083#1072 +\n" +
                "          #1085#1091'1)'\n" +
                "        DataBinding.FieldName = 'STATE_GP_NAME'\n" +
                "      end";
        XMLBuilder instance = new XMLBuilder("", "", null);
        String expResult = "#1057#1090#1072#1090#1091#1089#1099' '#1086#1073#1098#1077#1082#1090#1086#1074' '#1082#1072#1087#1080#1090#1072#1083#1100#1085#1086#1075#1086' '#1089#1090#1088#1086#1080#1090#1077#1083#1100#1089#1090#1074#1072' ('#1087#1086' '#1075#1077#1085#1077#1088#1072#1083#1100#1085#1086#1084#1091' '#1087#1083#1072#1085#1091'1)'";
        String result = instance.getCaption(field);
        assertEquals(expResult, result);
    }

    /**
     * Test of decodeCaption method, of class XMLBuilder.
     */
    @Test
    public void testDecodeCaption() {
        System.out.println("decodeCaption");
        String caption = "#1057#1090#1072#1090#1091#1089#1099' '#1086#1073#1098#1077#1082#1090#1086#1074' '#1082#1072#1087#1080#1090#1072#1083#1100#1085#1086#1075#1086' '#1089#1090#1088#1086#1080#1090#1077#1083#1100#1089#1090#1074#1072' ('#1087#1086' '#1075#1077#1085#1077#1088#1072#1083#1100#1085#1086#1084#1091' '#1087#1083#1072#1085#1091'1)'";
        XMLBuilder instance = new XMLBuilder("", "", null);
        String expResult = "Статусы объектов капитального строительства (по генеральному плану1)";
        String result = instance.decodeCaption(caption);
        assertEquals(expResult, result);
    }
    
}
